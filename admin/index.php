<?php 
  session_start();
  include('../config.php');
  include('../lib/db.php');
  include('../lib/users.php');
  include('../til/til.php');

  $view = $_GET['view'] ?? 'index';
  $til = $_SESSION['til'] ?? 'uz';

  if(isAuths()){
    include($config['base']['path']."views/admin_layouts/main.php");
  }
  else{
    include($config['base']['path']."views/admin_pages/login.php");
  }


?>