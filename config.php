<?php 
	$config = [
		'db' => [
			'host' => 'localhost',
			'username' => 'root',
			'userpassword' => '',
			'dbName' => 'textil_base',
			'tablePrefix' => 'textil_',
		],

		'base' => [
			'path' => $_SERVER['DOCUMENT_ROOT']."/textil_prect/",
			'url' => 'http://localhost/textil_prect/',
		]
	];

	const Url = 'http://localhost/textil_prect/';

?>