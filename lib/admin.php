<?php 
	/**
	 * Kirish
	 */
	function login($login,$password){
		$user = checkUser($login,$password);
		if(!$user){
			$_SESSION['error_uz'] = "Siz login va/yoki fishni noto`g`ri kiritdingiz";
			$_SESSION['error_ru'] = "Вы ввели неверный логин и / или прибор";
			$_SESSION['error_en'] = "You have entered an invalid login and / or fixture";
			return false;
		}
		else{
			$_SESSION['ism'] = $login;
			$_SESSION['login'] = $password;
			
			return true;
		}
	}

	/**
	 * Foydalanuvchi avtorizatsiyadan o`tgan yoki o`tmaganligini tekshirish funksiyasi
	 */
	function isAuth(){
		$login = $_SESSION['ism'] ?? null;
		$password = $_SESSION['login'] ?? null;
		if(checkUser($login,$password)>0)
			return true;
		return false;
	}

	/**
	 * Chiqish
	 */
	function logout(){
		// session_start();
		session_unset();
		session_destroy();
		return true;
	}
?>