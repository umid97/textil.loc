<?php 
	function connection(){
		global $config;
		$db = new mysqli($config['db']['host'], $config['db']['username'], $config['db']['userpassword'], $config['db']['dbName'])or die($db->error);
		$db->query("SET NAMES utf8");
		return $db;
	}

	function getTablePrefix($a){
		global $config;
		return $config['db']['tablePrefix'].$a;
	}

	function getArray($row){
		$array = [];
		$i = 0;
		while($r = $row->fetch_array()){
			$array[$i] = $r;
			$i++;
		}
		return $array;
	}

	function getAll($a){
		$db = connection();
		$sql = $db->query("SELECT * FROM ".getTablePrefix($a))or die($db->error);
		return getArray($sql);
	}

	function getAllMahsulot($a){
		$db = connection();
		$sql = $db->query("SELECT * FROM ".getTablePrefix($a)." ORDER BY id desc")or die($db->error);
		return getArray($sql);
	}

	function getId($a, $col, $id){
		$db = connection();
		$sql = $db->query("SELECT * FROM ".getTablePrefix($a)." WHERE $col = {$id}")or die($db->error);
		$r = $sql->fetch_array();
		if($r)
			return true;
		return false;
	}

	function getOneCol($a, $col, $id, $lim = 15){
		$db = connection();
		$sql = $db->query("SELECT * FROM ".getTablePrefix($a)." WHERE $col = '".$id."' ORDER BY id DESC LIMIT $lim ")or die($db->error);
		return getArray($sql);
	}

	// foydalanuvchi qo'shish
	function getInsert($name, $f, $tel, $manzil, $login){
		$db = connection();
		$f = htmlspecialchars(addslashes($f));
		$tel = htmlspecialchars(addslashes($tel));
		$manzil = htmlspecialchars(addslashes($manzil));
		$login = htmlspecialchars(addslashes($login));
		$sql = $db->query("SELECT * FROM ".getTablePrefix($name)." WHERE login = '".$login."'")or die($db->error);
		$r = $sql->num_rows;
		if($r > 0)
		{
			$sql = false;
		}
		else{
			$sql = $db->query("INSERT INTO ".getTablePrefix($name)." (fish, tel, manzil, login, vaqt) VALUES ('{$f}', '{$tel}', '{$manzil}','{$login}', NOW())")or die($db->error);
		}
		if($sql)
			return true;
		return false;
	}

	function getStatus($val){
		$val = htmlspecialchars(addslashes($val));
		$db = connection();
		$sql = $db->query("SELECT * FROM textil_contact WHERE login ='".$val."' ")or die($db->error);
		$n = $sql->num_rows;
		if($n > 0)
			return true;
		return false;
	}
	function checkUser($fish,$login){
		$db = connection();
		$fish = htmlspecialchars(addslashes($fish));
		$login = htmlspecialchars(addslashes($login));
		$result = $db->query("SELECT COUNT(*) as soni FROM textil_contact WHERE fish='".$fish."' AND login='".$login."'") or die("xatolik: ".$db->error);
		$row = $result->fetch_array();	

		if($row['soni'])
			return true;

		return false;
	}

	function checkUsers($login, $parol){
		$login = htmlspecialchars(addslashes($login));
		$parol = htmlspecialchars(addslashes($parol));
		$db = connection();
		$sql = $db->query("SELECT * FROM textil_admin WHERE login='".$login."' AND parol='".$parol."'")or die("Xatoli mavjud:".$db->error);
		$r = $sql->num_rows;
		if($r > 0)
			return true;
		return false;
	}


	// malumot sonini ko'rish
	function getZakaz($table, $col, $name, $a = null, $b = null){
		$db = connection();
		if($a == null){
			$sql = $db->query("SELECT count(*) as soni FROM ".getTablePrefix($table)." WHERE $col = '".$name."' ")or die($db->error);
			$r = $sql->fetch_array();
			return $r['soni'];
		}
		else{
			$sql = $db->query("SELECT count(*) as soni FROM ".getTablePrefix($table)." WHERE $col = '".$name."' AND $b = '".$a."' ")or die($db->error);
			$r = $sql->fetch_array();
			return $r['soni'];
		}
		
	}

	// bitta  malumot olish
	function getOneInfo($t, $col, $id, $a = null, $b = null){
		$db = connection();
		if($a == null){
			$sql = $db->query("SELECT * FROM ".getTablePrefix($t)." WHERE $col = '".$id."' ")or die($db->error);
			return getArray($sql);
		}
		else{
			$sql = $db->query("SELECT * FROM ".getTablePrefix($t)." WHERE $col = '".$id."' AND $b = '".$a."' ")or die($db->error);
			return getArray($sql);
		}
		
	}


	// buyurtmani berish 
	function getBuyurtma($fish, $bolim, $izoh, $tel, $login, $manzil){
		$db = connection();
		$fish = htmlspecialchars(addslashes($fish));
		$bolim = htmlspecialchars(addslashes($bolim));
		$izoh = htmlspecialchars(addslashes($izoh));
		$tel = htmlspecialchars(addslashes($tel));
		$login = htmlspecialchars(addslashes($login));
		$manzil = htmlspecialchars(addslashes($manzil));

		$sql = $db->query("INSERT INTO textil_zakaz (fish, mahsulot_nomi, status, izoh, vaqt, tel, login, manzil) VALUES ('{$fish}', '{$bolim}', 'active', '{$izoh}', NOW(), '{$tel}', '{$login}', '{$manzil}')")or die($db->error);
		if($sql)
			return true;
		return false;
	}

	function getAllRandom($name, $col, $val, $a){
		$db = connection();
		$sql = $db->query("SELECT * FROM ".getTablePrefix($name)." WHERE $col = '".$val."'")or die($db->error);
		$r = $sql->fetch_array();
		return $r[$a];
	}


	function AllCount($a='textil_mahsulotlar'){
		$db = connection();
		$sql = $db->query("SELECT * FROM ".getTablePrefix($a));
		$r = $sql->num_rows;
		return $r;
	}

	function Soni(){
		$db = connection();
		$sql = $db->query("SELECT * FROM textil_mahsulotlar");
		$r = $sql->num_rows;
		return $r;
	}

	function AllCounts($name, $a = 'active'){
		$db = connection();
		$sql = $db->query("SELECT * FROM ".getTablePrefix($name)." WHERE status = '{$a}' ");
		$r = $sql->num_rows;
		return $r;
	}

	function PaginationViews(){
		$soni = Soni();
		$page = ceil($soni / 5);
		define('first', 5);
		$s = 0;
		?>
			<nav>
				<ul class="pagination pagination-sm">
					<?php for($i = 1; $i <= $page; $i++): ?>
						
						<li>
						<a class="btn btn-info btn-sm" href="<?=URLMANAGER.'/'.$s?>"><?=$i;?></a>
						</li>
						<?php $s = $s + first; ?>
					<?php endfor; ?>
				</ul>
			</nav>
		<?php
	}

	function getInfoAll($a = 0){
		$db = connection();
		$sql = $db->query("SELECT * FROM textil_mahsulotlar ORDER BY id DESC limit {$a}, 5");
		return getArray($sql);
	}


	function getThere($limit, $a=null){
		$db = connection();
		if($a == null){
			$sql = $db->query("SELECT * FROM textil_mahsulotlar ORDER BY id desc limit {$limit} ")or die($db->error);
			return getArray($sql);
		}
		else{
			$sql = $db->query("SELECT * FROM textil_mahsulotlar WHERE subbolim_nomi = '{$a}' ORDER BY id desc limit {$limit} ")or die($db->error);
			return getArray($sql);
		}
	}


	function getXabar($f, $text, $login){
		$f = htmlspecialchars(addslashes($f));
		$text = htmlspecialchars(addslashes($text));
		$login = htmlspecialchars(addslashes($login));
		$db = connection();
		$sql = $db->query("INSERT INTO textil_xabar (fish, status, xabar, vaqt, login) VALUES('{$f}', 'active', '{$text}', NOW(), '{$login}')")or die();
		if($sql)
			return true;
		return false;
	}

	function getsLimits($n, $limit){
		$db = connection();
		$sql = $db->query("SELECT * FROM ".getTablePrefix($n)." ORDER BY id DESC LIMIT $limit ")or die($db->error);
		return getArray($sql);
	}

	// mahsulot qoshish
	function addMahsulot($nomi_uz, $nomi_en, $nomi_ru, $izoh_uz, $izoh_ru, $izoh_en, $rasm1, $rasm2, $narx, $bolim, $chegirma){
		global $config;
		$nomi_uz = htmlspecialchars(addslashes($nomi_uz));
		$nomi_ru = htmlspecialchars(addslashes($nomi_ru));
		$nomi_en = htmlspecialchars(addslashes($nomi_en));
		$izoh_uz = htmlspecialchars(addslashes($izoh_uz));
		$izoh_ru = htmlspecialchars(addslashes($izoh_ru));
		$izoh_en = htmlspecialchars(addslashes($izoh_en));
		$rasm1 = htmlspecialchars(addslashes($rasm1));
		$rasm2 = htmlspecialchars(addslashes($rasm2));
		$bolim = htmlspecialchars(addslashes($bolim));
		$chegirma = htmlspecialchars(addslashes($chegirma));
		$a = time().$_FILES["{$rasm1}"]['name'];
		$b = time().$_FILES["{$rasm2}"]['name'];
		$url = $config['base']['path']."web/images/";
		
		move_uploaded_file($_FILES["{$rasm1}"]['tmp_name'], $url.$a);
		move_uploaded_file($_FILES["{$rasm2}"]['tmp_name'], $url.$b);
		
		$db = connection();
		$sql = $db->query("INSERT INTO textil_mahsulotlar (nomi_uz, nomi_en, nomi_ru, izoh_uz, izoh_ru, izoh_en, rasm1, rasm2, narx, vaqt, subbolim_nomi, status, chegirma) VALUES('{$nomi_uz}', '{$nomi_en}', '{$nomi_ru}', '{$izoh_uz}', '{$izoh_ru}', '{$izoh_en}', '{$a}', '{$b}', '{$narx}', NOW(), '{$bolim}', 'active', '{$chegirma}')")or die($db->error);
		if($sql)
			return true;
		return false;
	}

	function getdelete($name, $col, $id){
		$db = connection();
		$sql = $db->query("DELETE FROM ".getTablePrefix($name)." WHERE $col = $id ")or die($db->error);
		if($sql)
			return true;
		return false;
	}

	function getUpdate($nomi_uz, $nomi_ru, $nomi_en, $izoh_uz, $izoh_ru, $izoh_en,$narx, $bolim, $chegirma, $id){

		global $config;
		$url = $config['base']['path']."web/images/";
		if($_FILES['rasm1']['name']){
			$img1 = $_FILES['rasm1']['name'];
			$img1 = time().$img1;
			move_uploaded_file($_FILES["rasm1"]['tmp_name'], $url.$img1);
		}

		if($_FILES['rasm2']['name']){
			$img2 = $_FILES['rasm2']['name'];
			$img2 = time().$img2;
			move_uploaded_file($_FILES["rasm2"]['tmp_name'], $url.$img2);
 		}
 		if($_FILES['rasm1']['error']){
 			$img1 = $_POST['img1'];
 		}
 		if($_FILES['rasm2']['error']){
 			$img2 = $_POST['img2'];
 		}

		$db = connection();
		
		$nomi_uz = htmlspecialchars(addslashes($nomi_uz));
		$nomi_ru = htmlspecialchars(addslashes($nomi_ru));
		$nomi_en = htmlspecialchars(addslashes($nomi_en));
		$izoh_uz = htmlspecialchars(addslashes($izoh_uz));
		$izoh_ru = htmlspecialchars(addslashes($izoh_ru));
		$izoh_en = htmlspecialchars(addslashes($izoh_en));

		$bolim = htmlspecialchars(addslashes($bolim));
		$chegirma = htmlspecialchars(addslashes($chegirma));
		
		
		
		$sql = $db->query("UPDATE textil_mahsulotlar SET nomi_uz = '{$nomi_uz}', nomi_ru = '{$nomi_ru}', nomi_en = '{$nomi_en}', izoh_uz = '{$izoh_uz}', izoh_ru = '{$izoh_ru}', izoh_en = '{$izoh_en}', rasm1 = '{$img1}', rasm2 = '{$img2}', narx = '{$narx}', vaqt = NOW(), subbolim_nomi = '{$bolim}', status = 'active', chegirma = '{$chegirma}' WHERE id = $id")or die($db->error);
		if($sql)
			return true;
		return false;
	}


	function GetWhere($a, $col, $val){
		$db = connection();
		$sql = $db->query("SELECT * FROM ".getTablePrefix($a)." WHERE $col = '".$val."' ")or die($db->error);
		return getArray($sql);
	}

	function getUpdateZakaz($s, $d=null, $id){
		$db = connection();
		$sql = $db->query("UPDATE textil_zakaz SET status = '{$s}', tamom_vaqt = '{$d}' WHERE id = $id ")or die($db->error);
		if($sql)
			return true;
		return false;
	}


	function getSession(){
		$db = connection();
		$sql = $db->query('SELECT * FROM textil_contact WHERE status = "" ');
	}


	function getInsertsXabar($fish, $xabar, $login){
		$db = connection();
		$xabar = htmlspecialchars(addslashes($xabar));
		$login = htmlspecialchars(addslashes($login));
		$fish = htmlspecialchars(addslashes($fish));

		$sql = $db->query("INSERT INTO textil_xabar(fish, status, xabar, vaqt, login) VALUES ('{$fish}', '{$xabar}', '{$login}')")or die($db->error);
		if($sql)
			return true;
		return false;
	}


	function getInsertsXabarAdmin($xabar, $login){
		$db = connection();
		$xabar = htmlspecialchars(addslashes($xabar));
		$login = htmlspecialchars(addslashes($login));

		$sql = $db->query("INSERT INTO  textil_xabar_admin (xabar, status, login_nomi, vaqt) VALUES ('{$xabar}', 'active', '{$login}', NOW())")or die($db->error);
		if($sql)
			return true;
		return false;
	}

	function getUpdatesXabar($a,$id){
		$db = connection();
		$sql = $db->query("UPDATE textil_xabar SET status = '{$a}' WHERE id = $id ")or die($db->error);
		if($sql)
			return true;
		return false;
	}

	function getSmsStatus($name, $col, $login, $col1, $active){
		$db = connection();
		$sql = $db->query("SELECT count(*) as soni FROM ".getTablePrefix($name)." WHERE $col = '{$login}' AND $col1 = '{$active}' ")or die($db->error);
		$r = $sql->fetch_array();
		return $r['soni'];
		}

	function getAdminSMS($name, $col, $login, $col1, $active){
		$db = connection();
		$sql = $db->query("SELECT * FROM ".getTablePrefix($name)." WHERE $col = '{$login}' AND $col1 = '{$active}' ")or die($db->error);
		return getArray($sql);
	}

	function getAdminCheck($login, $parol){
		$login = htmlspecialchars(addslashes($login));
		$parol = htmlspecialchars(addslashes($parol));
		$db = connection();
		$sql = $db->query("SELECT count(*) as son FROM textil_admin WHERE login = '{$login}' AND parol = '{$parol}' ")or die($db->error);
		$r = $sql->fetch_array();
		if($r['son'] > 0)
			return true;
		return false;
	}

	function updateParol($login, $parol){
		$db = connection();
		$sql = $db->query("UPDATE textil_admin SET login = '{$login}', parol = '{$parol}' WHERE id = 1 ")or die($db->error);
		if($sql)
			return true;
		return false;
	}

	
?>
