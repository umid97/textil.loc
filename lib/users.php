<?php 
	/**
	 * Kirish
	 */
	function session($login,$password){

		if(!checkUsers($login,$password)){
			$_SESSION['error1_uz'] = "Siz login va/yoki parolni noto`g`ri kiritdingiz";
			$_SESSION['error1_ru'] = "Вы ввели неверный логин и / или пароль";
			$_SESSION['error1_en'] = "You have entered the login and / or password incorrectly";
			return 'false';
		}
		else{
			$_SESSION['login'] = $login;
			$_SESSION['parol'] = $password;
			
			return 'true';
		}
		
	}

	/**
	 * Foydalanuvchi avtorizatsiyadan o`tgan yoki o`tmaganligini tekshirish funksiyasi
	 */
	function isAuths(){
		$login = $_SESSION['login'] ?? 'null';
		$password = $_SESSION['parol'] ?? 'null';
		if(checkUsers($login,$password))
			return true;
		return false;
	}

	/**
	 * Chiqish
	 */
	function logout(){
		session_unset();
		session_destroy();
		return true;
	}
?>