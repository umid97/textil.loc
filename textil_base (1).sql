-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июл 25 2019 г., 15:11
-- Версия сервера: 10.1.31-MariaDB
-- Версия PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `textil_base`
--

-- --------------------------------------------------------

--
-- Структура таблицы `textil_admin`
--

CREATE TABLE `textil_admin` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `parol` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `textil_admin`
--

INSERT INTO `textil_admin` (`id`, `login`, `parol`) VALUES
(1, 'umidjon', 'umidjon'),
(2, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `textil_bolimlar`
--

CREATE TABLE `textil_bolimlar` (
  `id` int(11) NOT NULL,
  `nomi_uz` varchar(255) NOT NULL,
  `nomi_ru` varchar(255) NOT NULL,
  `nomi_en` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `textil_bolimlar`
--

INSERT INTO `textil_bolimlar` (`id`, `nomi_uz`, `nomi_ru`, `nomi_en`) VALUES
(1, 'Bosh sahifa', 'Домашняя страница\r\n', 'Home Page'),
(2, 'Bolimlar ', 'Pазделы', 'Departments'),
(3, 'Mahsulotlar ', 'продукты', 'Products'),
(4, 'Biz bilan aloqa', 'Свяжитесь с нами', 'Contact us'),
(5, 'Yordam ', 'помощь', 'help');

-- --------------------------------------------------------

--
-- Структура таблицы `textil_contact`
--

CREATE TABLE `textil_contact` (
  `id` int(11) NOT NULL,
  `fish` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `manzil` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `vaqt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `textil_contact`
--

INSERT INTO `textil_contact` (`id`, `fish`, `tel`, `manzil`, `login`, `vaqt`) VALUES
(1, 'umidjon', '99897-416-97-09', 'salom manzilim', 'umidjon', ''),
(2, 'nodirbek', '+99891-120-33-00', 'salom yaxshimisizlar', 'nodirbek', '2019-07-10 10:31:57'),
(4, 'sardor', '99897-416-97-09', 'salom', 'users', '2019-07-12 10:16:23'),
(5, 'umidjon', '+99891-120-33-00', 'asdasd', 'kkk', '2019-07-12 10:20:14'),
(6, 'umidjon', '936403009', 'asdasda', 'aa', '2019-07-12 10:22:09'),
(7, 'umidjon', '+99894-556-54-78', 'salom yaxshimisizlar', 'assas', '2019-07-12 10:30:25'),
(8, 'Karimojon Aliqov', '999956693', 'salom', 'nnnn', '2019-07-12 10:36:17'),
(9, 'nn', '936403009', 'asadasd', 'dasturchi', '2019-07-12 10:39:28'),
(10, 'asdadaadasda', '9876541', 'adasd', 'asdasdasd', '2019-07-12 10:40:38'),
(11, 'dd', '12345678', 'dddd', 'dsdsds', '2019-07-12 10:44:47'),
(12, 'ahrorbek', '+99894-556-54-78', 'lasdjkasjfkj', 'ahrorbek', '2019-07-15 09:13:49'),
(13, 'javohirbek', '936403009', 'adaldkal', 'ddd', '2019-07-16 15:38:50'),
(14, 'umidjon', '132', 'farg\'ona ', 'use', '2019-07-17 11:18:36'),
(15, 'karimjon', '123456', 'asdasda', 'asdf', '2019-07-17 11:33:30'),
(16, 'jaloliddin', '1234568', 'Qo\'qon', 'aaa', '2019-07-17 11:36:15'),
(20, 'Karimjon', '7579897', 'Andijon shahar', 'dd', '2019-07-23 15:33:38'),
(21, 'mm', '123456789', 'Farg\'ona shahar', 'fergana', '2019-07-24 10:14:33'),
(22, '', '+998914523695', 'salom men o\'tkirbek', 'tatuff', '2019-07-24 18:30:54'),
(23, 'Nodirbek', '1203300', 'Marg\'ilon shahar', 'sirius', '2019-07-25 06:59:47'),
(25, '', '456465', '4adasdasda', '4a56sdasd', '2019-07-25 07:10:36'),
(26, 'adsasda', 'sasdasd', 'dasdasd', 'asdasda', '2019-07-25 07:11:57'),
(27, 'adlasj', 'kjdsklfjds', 'jlksdfjskl', 'sjdkfjskdljfl', '2019-07-25 07:12:41'),
(28, 'Azizbek', '1234569', 'oltiariq', '123456', '2019-07-25 09:36:51');

-- --------------------------------------------------------

--
-- Структура таблицы `textil_mahsulotlar`
--

CREATE TABLE `textil_mahsulotlar` (
  `id` int(11) NOT NULL,
  `nomi_uz` varchar(255) NOT NULL,
  `nomi_en` varchar(255) NOT NULL,
  `nomi_ru` varchar(255) NOT NULL,
  `izoh_uz` text NOT NULL,
  `izoh_ru` text NOT NULL,
  `izoh_en` text NOT NULL,
  `rasm1` varchar(255) NOT NULL,
  `rasm2` varchar(255) NOT NULL,
  `narx` varchar(255) NOT NULL,
  `vaqt` date NOT NULL,
  `subbolim_nomi` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `chegirma` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `textil_mahsulotlar`
--

INSERT INTO `textil_mahsulotlar` (`id`, `nomi_uz`, `nomi_en`, `nomi_ru`, `izoh_uz`, `izoh_ru`, `izoh_en`, `rasm1`, `rasm2`, `narx`, `vaqt`, `subbolim_nomi`, `status`, `chegirma`) VALUES
(28, 'Yangi bolalar kiyimlari', 'New baby clothes', 'Новая детская одежда', 'Yangiliklar chegirmalar. Bolalalar uchun yangi kiyimlar 2 yoshdan 14 yoshgacha kiyimlar. Futbolka mayka sho\'rtiklar. Chegirmalari 15% Asl mahsulot O\'z xaridingizdan mamnun bo\'lasiz!', 'Новости скидки. Новые пальто для малышей Одежда от 2 до 14 лет. Футболки с футболкой. Скидки 15% Подлинный продукт удовлетворенности!\r\n', 'News discounts. New coats for babies Clothing from 2 to 14 years. T-shirts with t-shirt. Discounts 15% Genuine Product Satisfaction!\r\n', '1563801117r2aa.jpg', '1563801117r.jpg', '150000', '2019-07-22', 'Yosh bolalar kiyimlari', 'active', '10'),
(29, 'Atlas mahsulotlari', 'Atlas products', 'Атлас продукты', 'Assalomu alaykum forumdoshlar.\r\nSizlardan bir mavzuda- atlas mavzusida qo\'lingizda bor materiallarni (qog\'ozdagisini, atlasnimas: )) bu mavzuda jamlashingizni yoki menga internet \r\nadresini berishingizni iltimos qilmoqchiydim. \r\nSababi, Turkiyada Mustaqil', 'Привет и мои друзья.\r\nНа одну из ваших тем - материалы, которые у вас есть на руках по теме атлас (бумага, атласнимы :))\r\nЯ хотел бы попросить вас дать мне свой адрес.\r\nПотому что я хотел написать статью в журнале, опубликованном в Турции в поддержку неза', 'Hello and my friends.\r\nOn one of your topics - the materials that you have in hand on the subject of the satin (paper, atlasnimas:))\r\nI\'d like to ask you to give me your address.\r\nBecause I wanted to write an article in a magazine published in Turkey in s', '1563781263atlas.jpg', '1563781263atlass.jpg', '50000', '2019-07-22', 'Trikatej mahsulotlar', 'active', '5'),
(30, 'Yangi kelinlar uchun liboslar', 'Clothes for New Brides', 'Одежда для Новобрачных', 'O’zbek qizlari – juda go’zal, maftunkor, odobli axloqli qizlar, ularni ta’riiflashga til ojiz. Milliy kelin libosida yurgan o’zbek kelinlarini aytmasa ham bo’laveradi. Bugun sizga foto kolleksiyamizning 2-to’plamini ko’rishga taklif etamiz.', 'Узбекские девушки очень красивые, обаятельные, справедливые девушки и не могут быть маркированы. Узбекские невесты, одетые в национальные наряды невесты, сказать не могут. Сегодня мы приглашаем вас посмотреть 2-ю коллекцию нашей коллекции фотографий.\r\n', 'The Uzbek girls are very beautiful, charming, fair-minded girls, and can not be labeled. The Uzbek brides, who are dressed in the national dress of bride, can not say. Today, we invite you to see the 2 nd collection of our photo collection.\r\n', '1563781919libos.jpg', '1563781919llll.jpg', '145000', '2019-07-22', 'Trikatej mahsulotlar', 'active', '10'),
(31, 'Bahmal gilamlar', 'Awesome carpets', 'Потрясающие ковры', 'Matoning o\'ziga xosligi : to\'qimaning qoldiq yo\'nalishiga va chuqurlik chuqurligiga bog\'liq bo\'lgan soyalar modullari  \r\n: mato tarkibi: ipak \r\nmato kengligi: 36 sm  \r\nqotish uzunligi: 1 mm \r\n* matoning narxi 1 metr uchun ko\'rsatilgan.\r\n\r\nVelvet matoni buyurtma savatiga qo\'shganda buyurtma miqdorini o\'zgartirib, uzluksiz sotib olinishi mumkin. \r\n\r\nIpak va paxta tana kontseptsiyasi jihatidan boshqa materiallardan ancha ustundir. Tabiiy tolalardan tayyorlangan mato inson tanasi uchun optimal mikroiqlimni saqlab turishga qodir.\r\n\r\nVelvet kiyim-kechak dizayni, ichki bezatish va qo\'shimcha aksessuarlar ishlab chiqarish uchun foydalanish mumkin - sharflar, poyabzal, sumkalar va bosh kiyimlar. \r\n\r\nMashinani 30 - 40 daraja yuvish mumkin. Mato parchalanmaydi, tushmaydi, rangi yorqinligini yo\'qotmaydi.\r\n\r\n** Monitörünüzdeki matoning rangi haqiqiy har xil bo\'lishi mumkin.', '\r\nУникальность ткани: Модули теней в зависимости от остаточного направления ткани и глубины глубины\r\n: состав ткани: шелк\r\nширина ткани: 36 см\r\nДлина ножниц: 1 мм\r\n* Стоимость ткани 1 метр.\r\n\r\nДобавление бархатной ткани в корзину заказа может изменить количество заказа и может быть приобретено непрерывно.\r\n\r\nШелк и хлопок намного превосходят другие материалы с точки зрения концепции тела. Натуральная тканевая ткань способна поддерживать оптимальный микроклимат для организма человека.\r\n\r\nБархат можно использовать для дизайна одежды, внутренней отделки и аксессуаров - шарфы, туфли, сумки и шапки.\r\n\r\nВы можете мыть машину при 30 - 40 градусах. Ткань не ломается, не уменьшается, цвет не теряет яркости.\r\n\r\n** Цвет ткани на вашем мониторе может отличаться от фактического.', '\r\nUniqueness of the fabric: Modules of shadows, depending on the residual direction of the tissue and the depth of the depth\r\n: fabric composition: silk\r\nfabric width: 36 cm\r\nShear length: 1 mm\r\n* The cost of the fabric is 1 meter.\r\n\r\nAdding the velvet fabric to the order basket may change the order quantity and can be purchased continuously.\r\n\r\nSilk and cotton are far superior to other materials in terms of body conception. The natural fabric fabric is able to maintain the optimal microclimate for the human body.\r\n\r\nVelvet can be used for garment design, interior decoration and accessories - scarves, shoes, bags and hats.\r\n\r\nYou can wash the machine at 30 - 40 degrees. The fabric does not break down, it does not diminish, its color does not lose its brightness.\r\n\r\n** The color of the fabric on your monitor may differ from actual', '1563809267bax.jpg', '1563809267bxax.jpg', '150000', '2019-07-22', 'Bahmal mahsulotlar', 'active', '10'),
(32, 'Baxmal adraslar', 'Бахмал Адрас', 'Bakhmal adras', 'Matoning o\'ziga xosligi : to\'qimaning qoldiq yo\'nalishiga va chuqurlik chuqurligiga bog\'liq bo\'lgan soyalar modullari  \r\n: mato tarkibi: ipak \r\nmato kengligi: 36 sm  \r\nqotish uzunligi: 1 mm \r\n* matoning narxi 1 metr uchun ko\'rsatilgan.\r\n\r\nVelvet matoni buyurtma savatiga qo\'shganda buyurtma miqdorini o\'zgartirib, uzluksiz sotib olinishi mumkin. \r\n\r\nIpak va paxta tana kontseptsiyasi jihatidan boshqa materiallardan ancha ustundir. Tabiiy tolalardan tayyorlangan mato inson tanasi uchun optimal mikroiqlimni saqlab turishga qodir.\r\n\r\nVelvet kiyim-kechak dizayni, ichki bezatish va qo\'shimcha aksessuarlar ishlab chiqarish uchun foydalanish mumkin - sharflar, poyabzal, sumkalar va bosh kiyimlar. \r\n\r\nMashinani 30 - 40 daraja yuvish mumkin. Mato parchalanmaydi, tushmaydi, rangi yorqinligini yo\'qotmaydi.\r\n\r\n** Monitörünüzdeki matoning rangi haqiqiy har xil bo\'lishi mumkin.', '\r\nУникальность ткани: Модули теней в зависимости от остаточного направления ткани и глубины глубины\r\n: состав ткани: шелк\r\nширина ткани: 36 см\r\nДлина ножниц: 1 мм\r\n* Стоимость ткани 1 метр.\r\n\r\nДобавление бархатной ткани в корзину заказа может изменить количество заказа и может быть приобретено непрерывно.\r\n\r\nШелк и хлопок намного превосходят другие материалы с точки зрения концепции тела. Натуральная тканевая ткань способна поддерживать оптимальный микроклимат для организма человека.\r\n\r\nБархат можно использовать для дизайна одежды, внутренней отделки и аксессуаров - шарфы, туфли, сумки и шапки.\r\n\r\nВы можете мыть машину при 30 - 40 градусах. Ткань не ломается, не уменьшается, цвет не теряет яркости.\r\n\r\n** Цвет ткани на вашем мониторе может отличаться от фактического.', '\r\nUniqueness of the fabric: Modules of shadows, depending on the residual direction of the tissue and the depth of the depth\r\n: fabric composition: silk\r\nfabric width: 36 cm\r\nShear length: 1 mm\r\n* The cost of the fabric is 1 meter.\r\n\r\nAdding the velvet fabric to the order basket may change the order quantity and can be purchased continuously.\r\n\r\nSilk and cotton are far superior to other materials in terms of body conception. The natural fabric fabric is able to maintain the optimal microclimate for the human body.\r\n\r\nVelvet can be used for garment design, interior decoration and accessories - scarves, shoes, bags and hats.\r\n\r\nYou can wash the machine at 30 - 40 degrees. The fabric does not break down, it does not diminish, its color does not lose its brightness.\r\n\r\n** The color of the fabric on your monitor may differ from actual.', '1563809576aa.jpg', '1563809576bbb.jpg', '170000', '2019-07-22', 'Bahmal mahsulotlar', 'active', '5'),
(33, 'Ayollar liboslari', 'Women\'s clothing', 'Женская одежда', 'O‘zbekiston hududida yashagan ajdodlarimiz liboslari tabiatning shart-sharoitlari, hayot tarzi va an’analariga asosan shakllangan. Biz milliy liboslarning shakllanishi va rivojlanishi haqida turli manbaalardan yetarlicha tushinchaga ega bo‘lganmiz. Arxeologik yodgorliklar, devoriy suratlar, yozma manbaalar, qo’lyozma asarlarining miniatyuralari – bularning barchasi O’zbekiston hududida qadimgi davrlaridan tortib to bugunga qadar qanday kiyim kiyilganini aytib beradi. Biz o‘zbek milliy liboslarining butun tarixiy qadr-qimmatini tushunishingizga yordam beruvchi 11 muhim tarixiy faktlarni xronologik tartibda to‘pladik :\r\n\r\nO‘zbekistonning tarixiy-madaniy hududlaridan biri bo‘lmish Sopollitepada topilgan to‘qimachik uskunalari qoldiqlari eneolit davridan boshlab (er.av. 4-3 ming) to‘qimachilik yuqori darajada rivojlanganini bildiradi.\r\nMarkaziy Osiyo matolarini tayyorlash texnikasi qiyin bo‘lgan. Ayniqsa, bezaklar berish mushkul edi. Matolarni bo’yashda yo‘l-yo‘l va rang-barang abrali xira naqshlar bilan chizilgan matolar olish imkonini beruvchi ikki uslubdan foydalanilgan. Abraning ikkinchi nomi – ikat. Aynan shu nom bilan bu mato G’arbda mashhurdir. О‘zbek choponi haqidagi dastlabki qaydnomalar VI asr haykalining Qultegin sharafiga bitilgan turkiy run yozuvlarida topilgan.\r\nTurk xalqining kiyimlari ustki qismi o‘rta asr boshlaridan boshlab XIX asrning boshigacha o‘zgarmadi. Barcha hududlarida liboslarning bichilishi asosan bir xil edi. Kiyimlar uzun, ba’zi hollarda keng yenglar bilan bichilgan. Libosning yon qismi yurishga qulay bo‘lishi uchun.\r\n', 'Костюмы наших предков, живущих на территории Узбекистана, были сформированы на основе природы, образа жизни и традиций природы. У нас есть хорошее представление о различных источниках формирования и развития национальных костюмов. Археологические памятники, настенные росписи, письменные источники, миниатюры рукописей - все они описывают, какую одежду они носят на территории Узбекистана с древних времен до наших дней. Мы в хронологическом порядке собрали 11 важных исторических фактов, которые помогут вам понять историческую ценность узбекских национальных костюмов:\r\n\r\nОстатки текстильного оборудования, найденные в Сополитепе, одном из исторических и культурных районов Узбекистана, указывают на то, что текстиль вырос с эпохи энеолита (4-3 тыс. До н.э.).\r\nТехника изготовления среднеазиатской ткани была сложной. Особенно сложно украсить. Существует два способа покраски ткани, позволяющие окрашивать ткани абразивными орнаментами и окрашенными мотивами. Второе имя абранка - икат. Это же имя известно на Западе. Первые записи об узбекском костюме были найдены в тюркских прогонах скульптуры шестого века имени Культегина.\r\nВерхняя часть одежды турецкого народа не изменилась с раннего средневековья до начала девятнадцатого века. Во всех регионах платья были в основном одинаковыми. Одежда длинная, в некоторых случаях покрыта большими ушами. По стороне платья легко ходить.', 'The costumes of our ancestors living on the territory of Uzbekistan have been formed on the basis of the nature, lifestyle and traditions of nature. We have a good idea of ​​various sources of the formation and development of national costumes. Archaeological monuments, wall paintings, written sources, miniatures of manuscripts - all describe what clothes they wear in the territory of Uzbekistan from ancient times to the present. We have chronologically collected 11 important historical facts that help you understand the historical value of Uzbek national costumes:\r\n', '1563967208asas.jpeg', '1563967208aa.jpg', '145000', '2019-07-24', 'Ayollar liboslari', 'active', '5'),
(34, 'Yosh kelinlar uchun liboslar', 'Dresses for young brides', 'Платья для молодых невест', 'XIV asrning birinchi choragidan boshlab Markaziy Osiyoga mo‘g‘ullar bosqini natijasida hayotning boshqa sohalari kabi liboslar olamiga ham mo‘g‘ul-xitoy an’analari kirib kela boshladi. Hukmdorlar, saroy a’yonlari, davlat amaldorlari, shuningdek, mahalliy aholining soch turmaklari, liboslarida mo‘g‘ul-xitoy uslubi yaqqol sezilardi. Bu davrda erkaklar kiyimlarida kalta yengli, diagonal kesim, yondan belgacha bichimlar va ko‘krakda kashtachilik namunalari bo‘lgan. Ayollarning ustki kiyimi ochiq qirqimdagi liboslardan iborat bo‘lib, pastga qarab kengayib ketgan juda uzun yengdan iborat bo‘lgan. Bosh kiyimi esa tepa qismiga patlar o‘rnatilgan murakkab bichimdagi qalpoq bo‘lgan.\r\n\r\nXVI asrga qadar “paranji” atamasi erkaklar va ayollar kiyimlarini anglatgan. Movarounnahrdagi Temuriylar va Hindistonda Boburiylar hukmronligi davrida olimlar, davlat amalidagi shaxslar, diniy arboblar paranji taqishgan. Shayboniylar davriga kelib paranji olimlarning kiyimi sanalgan. Oqibatda, mazkur buyum uydan chiqayotganda ayollar kiyimining ajralmas atributiga aylandi.\r\n\r\nXIX asrda chor Rossiyasi tomonidan hududni mustamlaka qilinishi natijasida mintaqa fabrika matolari bilan to‘lib toshdi. Bu ommaviy sanoat ishlab chiqarishining rivojlanishiga olib keldi. Tikuv mashinalarining paydo bo‘lishi hududda liboslar tayyorlashda inqilobiy voqeaga aylandi. Natijada, an’anaviy keng, uzun liboslar qatorida qomatni ko‘rsatib turuvchi va murakkab bichimga ega yevropacha uslubdagi kiyim-kechaklar paydo bo‘la boshladi.\r\nXom-ashyoning ko‘pligi Movarounnahr shaharlarida turli to‘qimachilik turlarini rivojlantirishga yordam berdi. Mahalliy badiiy to‘qimachilik maktablari mavjud bo‘lib, ular mazku hunarning rivojlanishiga turtki bo‘ldi. Toshkent, Buxoro, Samarqand, Qo‘qon, Marg‘ilon, Namanganda 17 turdagi an’anaviy o‘zbek ipak matolari ishlab chiqarila boshladi.\r\n\r\nXX asrning oxiridan boshlab va XXI asrning boshlarida o‘zbekona naqshlar G‘arb dunyosida ham keng tarqala boshladi. 1966 yilda aktrisa Sofi Loren Vogue jurnalining noyabr sonida ikat choponida paydo bo‘ldi. 2009 yilda Gucci va Missoni modalar uylari o‘z to‘plamlarida bosma ikatdan foydalanishdi. Shundan beri o‘zbek ikati va xon-atlasi kiyim-kechak va zargarlik buyumlaridan boshlab interyergacha, dunyoning turli soha dizaynerlari tomonidan faol ishlatib kelinadi.', '\r\nС первой четверти XIV века до Монголии монгольско-китайские традиции стали проникать в мир одежды, а также в другие сферы жизни. Судьи, судебные чиновники, государственные служащие, а также прически местных жителей, одетые в китайском стиле в монгольском стиле. В этот период мужская одежда имела короткие рукава, диагональные разрезы, тапочки и образцы вышивки на груди. Верхняя одежда женщины состояла из платьев с открытым концом и длинного удлиненного шарнира. Его головной убор был сложной фигурой с шляпой на вершине.\r\n\r\nК 16 веку слово «паранджи» означало мужскую и женскую одежду. Тимуриды в Мовароуннахре и Бабуриды в Индии, ученые, общественные деятели и религиозные деятели имеют параны. Во время Shaybânis, платья ученых-паранджи были подсчитаны. В результате этот продукт стал незаменимым атрибутом женской одежды, выходя из дома.\r\n\r\nВ 19 веке в результате колонизации региона царской Россией регион был полон фабричных тканей. Это привело к развитию массового промышленного производства. Появление швейных машин стало революционным событием в производстве одежды в регионе. В результате, среди широкого ассортимента традиционных платьев, платья европейского стиля, которые показывают тело и имеют утонченный вид.\r\nОбилие сырья помогло освоить разнообразные ткани в городах Мовароуннахр. Есть местные художественные текстильные школы, и они способствовали развитию ремесла мажук. 17 традиционных традиционных узбекских шелковых тканей были изготовлены в Ташкенте, Бухаре, Самарканде, Коканде, Маргилане и Намангане.\r\n\r\nС конца 20-го и начала 21-го века узбекские узоры получили широкое распространение в западном мире. В 1966 году актриса Софи Лорен Vogue появилась на куртке в ноябре. В 2009 году дома моды Gucci и Missoni использовали собственные печатные этикетки. С тех пор узбекский икат и хан-атлас активно использовались различными дизайнерами отрасли - от одежды и украшений до интерьеров.', 'From the first quarter of the XIV century to Mongolia Mongolian-Chinese traditions began to penetrate into the world of clothing as well as other spheres of life. Judges, court officials, public officials, as well as locals\' hairstrips, dressed in Mongolian-style Chinese style. During this period, men\'s clothing had short sleeves, diagonal cuts, slippers and chest embroidery samples. The women\'s outer garment consisted of open-ended dresses and a long, long-widened hinge. His headdress was a sophisticated figure with a hat on the top.\r\n\r\nBy the 16th century the word &quot;paranji&quot; meant men\'s and women\'s clothing. Timurids in Movarounnahr and Baburids in India, scientists, public figures, and religious figures have paranages. At the time of the Shaybânîs, the dresses of paranji scholars were counted. As a result, this product has become an indispensable attribute of women\'s clothing while leaving the house.\r\n\r\nIn the 19th century as a result of the colonization of the region by Tsarist Russia, the region was full of factory fabrics. This has led to the development of mass industrial production. The appearance of sewing machines has become a revolutionary event in the production of clothing in the region. As a result, among the wide range of traditional dresses, the European-style dresses that show the body and have a sophisticated look.\r\nThe abundance of raw materials helped to develop a variety of textiles in the cities of Movarounnakhr. There are local artistic textile schools and they have been instrumental in the development of the mazhuq craft. 17 traditional traditional Uzbek silk fabrics were produced in Tashkent, Bukhara, Samarkand, Kokand, Margilan and Namangan.\r\n\r\nFrom the end of the 20th century and beginning of the 21st century, Uzbek patterns have become widespread in the Western world. In 1966, actress Sofi Loren Vogue appeared on the jacket in November. In 2009, Gucci and Missoni fashion houses used their own printing labels. Since then, Uzbek ikat and khan-atlas have been actively used by various industry designers from clothing and jewelry to interiors.\r\n', '1563967355bbb.jpg', '1563967355ewe.jpg', '185000', '2019-07-24', 'Ayollar liboslari', 'active', '0'),
(35, 'Qishki liboslar ayollar uchun', 'Winter clothes for women', 'Зимняя одежда для женщин', 'Duxoba ko‘ylak\r\nBu mavsumda duxoba – jinsi, top va jemperlar bilan birgalikda kundalik kiyiladigan liboslar qatoridan joy oldi.\r\n\r\nPalto\r\nPaltolar uchun har doimgidek eng ommabop, urfda bo‘lgan va go‘zal ranglar – och jigarrang va sarg‘ish ranglar hisoblanadi. Uzunligiga kelganda esa, qancha uzun bo‘lsa, shuncha issiq va uslubga muvofiq bo‘ladi.\r\n\r\nQalin sviter\r\nBu mavsumda eng so‘nggi urfda bo‘lgan kiyinish – bu issiq kiyinish. Tabiiy jundan, yumshoq va juda shinam sviterlar ayni muddao. Qanday uslubdagisini tanlash esa sizning didingizga bog‘liq.\r\n\r\nPuxovik (par kurtka)\r\nBiz bilamizki, bu mavsumda eng urf hisoblanuvchi issiq kiyim – puxoviklar. Eng ajoyib puxoviklarni topish qiyin bo‘lsa-da, iloji bor.\r\n\r\nTrikotaj\r\nBu mavsumning oldingilaridan farqi shuki, hozir faqat bluzkalar emas, balki trikotaj matodan tikilgan kombinezonlar, midi ko‘ylaklar, keng shimlar va dunyodagi eng qulay kostyumlar rasm bo‘lgan.', 'Духе рубашки\r\nВ течение этого сезона духобо - куртка, мяч и джемперы - среди повседневных костюмов.\r\n\r\nпальто\r\nНаиболее популярными, традиционными и красочными цветами для пальто являются светло-коричневые и желтоватые цвета. Пока это дольше, это будет более теплым и более современным.\r\n\r\nТолстый свитер\r\nОдеться в это последнее платье в этом сезоне жарко. Естественная влажность мягких и очень прохладных свитеров. Выбор стиля зависит от вашего вкуса.\r\n\r\nПуховик (куртка)\r\nМы знаем, что самые теплые платья в этом сезоне самые лучшие. Трудно найти самые захватывающие затяжки, хотя.\r\n\r\nтрикотажные изделия\r\nОтличие от предыдущего сезона в том, что это не просто блузки, а комбинации трикотажа, миди-рубашки, широких брюк и самых удобных костюмов в мире.', '\r\nDukhe shirts\r\nDuring this season, the dukhobo - the jacket, the ball and the jumpers - are among the daily costumes.\r\n\r\nCoat\r\nThe most popular, traditional and colorful colors for coats are light brown and yellowish colors. As long as it is longer, it will be warmer and more up to date.\r\n\r\nThick sweater\r\nDress up in this latest dress this season is hot. The natural moisture is soft and very cool sweaters. The choice of style is dependent on your taste.\r\n\r\nPuxovik (par jacket)\r\nWe know that the warmest dresses this season is the best. It\'s hard to find the most exciting puffs, though.\r\n\r\nKnitwear\r\nThe difference from the previous season is that it is not just blouses, but combinations of knitted fabric, midi shirts, wide trousers and the most comfortable suits in the world.', '1563967615asas.jpeg', '1563967615qq.jpg', '225000', '2019-07-24', 'Ayollar liboslari', 'active', '10'),
(36, 'Yangi bahmal mahsulotlar', 'New products', 'Новые продукты', 'Bahmal mahsulotlar. Yangi xildagi bahmal mahsulotlar. Gilamlar, ko\'ylaklar, astarliklar va boshqa mahsulotlar endi bizda.\r\nQulay va arzon narxlarda tez biz bilan bog\'laning va ajoyib sifatli mahsulotlarga ega bo\'ling', 'Удивительные продукты. Новые виды продукции. У нас есть ковры, рубашки, подкладки и многое другое.\r\nСвяжитесь с нами быстро и удобно по доступным и доступным ценам', 'Amazing products. New types of products. We have carpets, shirts, linings and more.\r\nContact us quickly and conveniently at affordable and affordable prices', '156396873000.jpg', '1563968730bxax.jpg', '225000', '2019-07-25', 'Bahmal mahsulotlar', 'active', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `textil_subbolims`
--

CREATE TABLE `textil_subbolims` (
  `id` int(11) NOT NULL,
  `nomi_uz` varchar(255) NOT NULL,
  `nomi_ru` varchar(255) NOT NULL,
  `nomi_en` varchar(255) NOT NULL,
  `bolim_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `textil_subbolims`
--

INSERT INTO `textil_subbolims` (`id`, `nomi_uz`, `nomi_ru`, `nomi_en`, `bolim_id`) VALUES
(1, 'Trikatej mahsulotlar', 'Трикотажные изделия', 'Knitted products', 2),
(2, 'Bahmal mahsulotlar', 'Удивительные продукты', 'Amazing products', 2),
(3, 'Yosh bolalar kiyimlari', 'Детская одежда', 'Children\'s clothing', 3),
(4, 'Ayollar liboslari', 'Женская одежда', 'Women\'s clothing\r\n', 3),
(5, 'Prastina va Padadiyannik', 'Прастина и Пададиник', 'Prastina and Padadianic', 3),
(6, 'Astarliklar ', 'грунтовка', 'Primers', 3),
(7, 'Mayka va futbolkalar', 'Футболки и футболки\r\n', 'T-shirts and T-shirts', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `textil_xabar`
--

CREATE TABLE `textil_xabar` (
  `id` int(11) NOT NULL,
  `fish` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `xabar` text NOT NULL,
  `vaqt` date NOT NULL,
  `login` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `textil_xabar`
--

INSERT INTO `textil_xabar` (`id`, `fish`, `status`, `xabar`, `vaqt`, `login`) VALUES
(5, 'karim', 'qabul', 'aslom', '2019-07-23', 'dd'),
(6, 'Umidjon', 'qabul', 'salomlar', '2019-07-24', 'umidjon'),
(8, 'Umidjon', 'qabul', 'Xa aka keldi raxmat sizga', '2019-07-24', 'umidjon'),
(9, 'Maftuna', 'qabul', 'Assalomu alaykum firmamizga kerek edi shunga komlpekt qanchadan bera olasiz?\r\n', '2019-07-24', 'fergana'),
(11, 'Nodirbek', 'qabul', 'Assalomu alaykum tezroq olib kela olasizlarmi aka.', '2019-07-25', 'sirius'),
(12, 'Azizbek', 'qabul', 'salom', '2019-07-25', '123456');

-- --------------------------------------------------------

--
-- Структура таблицы `textil_xabar_admin`
--

CREATE TABLE `textil_xabar_admin` (
  `id` int(11) NOT NULL,
  `xabar` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `login_nomi` varchar(255) NOT NULL,
  `vaqt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `textil_xabar_admin`
--

INSERT INTO `textil_xabar_admin` (`id`, `xabar`, `status`, `login_nomi`, `vaqt`) VALUES
(4, 'Va alaykum assalom', 'active', 'dd', '2019-07-23'),
(11, 'asdasdasa', 'active', 'dd', '2019-07-24'),
(12, 'yaxshmisizlar', 'active', 'dd', '2019-07-24'),
(15, 'xa uka tez kunlarda yetib boradi', 'active', 'sirius', '2019-07-25');

-- --------------------------------------------------------

--
-- Структура таблицы `textil_zakaz`
--

CREATE TABLE `textil_zakaz` (
  `id` int(11) NOT NULL,
  `fish` varchar(255) NOT NULL,
  `mahsulot_nomi` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `izoh` text NOT NULL,
  `vaqt` date NOT NULL,
  `tel` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `manzil` varchar(255) NOT NULL,
  `tamom_vaqt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `textil_zakaz`
--

INSERT INTO `textil_zakaz` (`id`, `fish`, `mahsulot_nomi`, `status`, `izoh`, `vaqt`, `tel`, `login`, `manzil`, `tamom_vaqt`) VALUES
(18, 'Umidjon', 'Yosh bolalar kiyimlari', 'qabul', 'salom yaxshimisizlar', '2019-03-08', '911203300', 'umidjon', 'Farg\'ona shahar', '2019-07-22'),
(19, 'umidjon', 'Trikatej mahsulotlar', 'tamom', 'salom', '2019-03-08', '123456456', 'admin', 'Qo\'qon shahar', '2019-07-22'),
(21, 'umidjon', 'Trikatej mahsulotlar', 'qabul', 'Kamplekt olmoqchiman', '2019-07-23', '911234558', 'umidjon', 'salom manzilim', '2019-07-23'),
(22, 'umidjon', 'Bahmal mahsulotlar', 'tamom', 'Shu materilada manga ko\'proq kerek edi shuning uchun sizga bo\'glanib olyamman', '2019-07-23', '97-416-97-09', 'umidjon', 'salom manzilim', '2019-07-23'),
(23, 'umidjon', 'Bahmal mahsulotlar', 'rad_etish', 'yyyyyyiiii\'o\'\'\'\'', '2019-07-23', '123123456', 'umidjon', 'salom manzilim', '2019-07-23'),
(24, 'Karimjon', 'Trikatej mahsulotlar', 'qabul', 'salom manga shu mahsulotdan kerek edi', '2019-07-23', '7579897', 'dd', 'Andijon shahar', '2019-07-24'),
(25, 'mm', 'Trikatej mahsulotlar', 'tamom', 'salomlar shu liboslar kerek edi manga', '2019-07-24', '123456789', 'fergana', 'Farg\'ona shahar', '2019-07-25'),
(26, 'Nodirbek', 'Bahmal mahsulotlar', 'active', 'Salom manga shu mahsulot kerek edi batafsil malumot olsam bo\'ladmi', '2019-07-25', '1203300', 'aa', 'Farg\'ona shahar Marg\'ilon tumani', '0000-00-00'),
(27, 'Nodirbek', 'Bahmal mahsulotlar', 'tamom', 'Salom manga shu mahsulot kerek edi kichik magazin bor narxini kelisha olamizmi?', '2019-07-25', '1203300', 'sirius', 'Marg\'ilon shahar', '2019-07-25'),
(28, 'adlasj', 'Yosh bolalar kiyimlari', 'rad_etish', 'yaxshimisizlar', '2019-07-25', '1254569', 'sjdkfjskdljfl', 'jlksdfjskl', '2019-07-25'),
(29, 'adlasj', 'Yosh bolalar kiyimlari', 'rad_etish', 'ashkdas', '2019-07-25', '12457987', 'sjdkfjskdljfl', 'jlksdfjskl', '2019-07-25'),
(30, 'adlasj', 'Yosh bolalar kiyimlari', 'rad_etish', 'dasda', '2019-07-25', 'sdaas', 'sjdkfjskdljfl', 'jlksdfjskl', '2019-07-25'),
(31, 'Azizbek', 'Trikatej mahsulotlar', 'active', 'dklaskjhsakj', '2019-07-25', '1234569', '123456', 'oltiariq', '0000-00-00'),
(32, 'umidjon', 'Trikatej mahsulotlar', 'qabul', 'Shu fasondan manga ham kerek edi batafsil gaplashib olsak bo\'ladimi?', '2019-07-25', '1203300', 'umidjon', 'salom manzilim', '2019-07-25');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `textil_admin`
--
ALTER TABLE `textil_admin`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `textil_bolimlar`
--
ALTER TABLE `textil_bolimlar`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `textil_contact`
--
ALTER TABLE `textil_contact`
  ADD PRIMARY KEY (`id`),
  ADD KEY `login` (`login`);

--
-- Индексы таблицы `textil_mahsulotlar`
--
ALTER TABLE `textil_mahsulotlar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subbolim_nomi` (`subbolim_nomi`);

--
-- Индексы таблицы `textil_subbolims`
--
ALTER TABLE `textil_subbolims`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bolim_id` (`bolim_id`),
  ADD KEY `nomi_uz` (`nomi_uz`);

--
-- Индексы таблицы `textil_xabar`
--
ALTER TABLE `textil_xabar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `login` (`login`);

--
-- Индексы таблицы `textil_xabar_admin`
--
ALTER TABLE `textil_xabar_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `login_nomi` (`login_nomi`);

--
-- Индексы таблицы `textil_zakaz`
--
ALTER TABLE `textil_zakaz`
  ADD PRIMARY KEY (`id`),
  ADD KEY `mahsulot_nomi` (`mahsulot_nomi`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `textil_admin`
--
ALTER TABLE `textil_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `textil_bolimlar`
--
ALTER TABLE `textil_bolimlar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `textil_contact`
--
ALTER TABLE `textil_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `textil_mahsulotlar`
--
ALTER TABLE `textil_mahsulotlar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT для таблицы `textil_subbolims`
--
ALTER TABLE `textil_subbolims`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `textil_xabar`
--
ALTER TABLE `textil_xabar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `textil_xabar_admin`
--
ALTER TABLE `textil_xabar_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `textil_zakaz`
--
ALTER TABLE `textil_zakaz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `textil_mahsulotlar`
--
ALTER TABLE `textil_mahsulotlar`
  ADD CONSTRAINT `textil_mahsulotlar_ibfk_1` FOREIGN KEY (`subbolim_nomi`) REFERENCES `textil_subbolims` (`nomi_uz`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `textil_subbolims`
--
ALTER TABLE `textil_subbolims`
  ADD CONSTRAINT `textil_subbolims_ibfk_1` FOREIGN KEY (`bolim_id`) REFERENCES `textil_bolimlar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `textil_xabar_admin`
--
ALTER TABLE `textil_xabar_admin`
  ADD CONSTRAINT `textil_xabar_admin_ibfk_1` FOREIGN KEY (`login_nomi`) REFERENCES `textil_xabar` (`login`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `textil_zakaz`
--
ALTER TABLE `textil_zakaz`
  ADD CONSTRAINT `textil_zakaz_ibfk_1` FOREIGN KEY (`mahsulot_nomi`) REFERENCES `textil_mahsulotlar` (`subbolim_nomi`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
