<?php 
	$array = [

		'win_uz' => 'Admin oynasi',
		'win_ru' => 'Окно администратора',
		'win_en' => 'Admin window',

		// ****
		'select_uz' => 'Tanlang',
		'select_en' => 'Select',
		'select_ru' => 'Bыбрать',

		// -----------------
		'btn_uz' => 'Qidiruv',
		'btn_en' => 'Search',
		'btn_ru' => 'поиск',

		// ------

		'aloqa_uz' => 'Biz bilan aloqa',
		'aloqa_ru' => 'Связаться с нами',
		'aloqa_en' => 'Contact Us',

		// ------
		'bolim_uz' => "Bo'limlar",
		'bolim_ru' => 'Pазделы',
		'bolim_en' => 'Departments',
		
		// ****

		'kabinet_uz' => 'Kabinet',
		'kabinet_ru' => 'Кабинет',
		'kabinet_en' => 'Cabinet',

		// *****

		'reg_uz' => 'Royxatdan otish',
		'reg_en' => 'Sign up',
		'reg_ru' => 'Список пользователей',

		// ****
		'login_uz' => 'Kirish',
		'login_ru' => 'Войти',
		'login_en' => 'Login',

		// ***
		'chiqish_uz' => 'Chiqish',
		'chiqish_ru' => 'выход',
		'chiqish_en' => 'Logout',

		// ****
		'placeholder_uz' => 'Qidirladigan so`zni kiriting...',
		'placeholder_en' => 'Enter the word you want ...',
		'placeholder_ru' => 'Введите слово, которое вы хотите ...',

		// ****
		'narx_uz' => 'Narx',
		'narx_ru' => 'Цена',
		'narx_en' => 'Price',

		// ****
		'xarid_uz' => "Eng ko'p xarid qilingan dasturlarimiz!",
		'xarid_ru' => "Самые популярные приложения!",
		'xarid_en' => 'Top purchased apps!',

		// ****
		'd_uz' => 'Dorixonalar uchun dasturlar',
		'd_en' => 'Software for pharmacies',
		'd_ru' => 'Программное обеспечение для аптекa',

		// ****
		'firma_uz' => 'Barcha turdagi firmalar uchun',
		'firma_ru' => 'Для всех типов фирмa',
		'firma_en' => 'For all types of firms',

		// ***

		'kurs_uz' => "Dasturlash bo'yicha kurslar",
		'kurs_ru' => 'Курсы программирования',
		'kurs_en' => 'Programming Courses',

		// *****
		'korish_uz' => "Ko'rish",
		'korish_ru' => 'вид',
		'korish_en' => 'Show',

		// ****
		'tit_uz' => 'Xarid soni',
		'tit_en' => 'Number of purchases',
		'tit_ru' => 'Количество покупок',

		// *****
		'm_uz' => "Yangi mahsulotlar",
		'm_en' => 'New products',
		'm_ru' => 'Новые продукты',

		// ******
		'm1_uz' => 'Bolalar kiyimlari',
		'm1_ru' => 'Детская одежда',
		'm1_en' => "Children's clothing",

		// ******
		'm2_uz' => 'Ayollar uchun liboslar',
		'm2_ru' => 'Женская одежда',
		'm2_en' => "Women's Clothing",

		// *****
		'bahmal_uz' => 'Bahmal mahsulotlar',
		'bahmal_ru' => 'Удивительные продукты',
		'bahmal_en' => 'Amazing products',

		// ****
		'fish_uz' => 'FISH',
		'fish_ru' => 'Фиш',
		'fish_en' => 'Fish',

		// ***
		'tel_uz' => 'Tel',
		'tel_en' => 'Tel',
		'tel_ru' => 'Тел',

		// ***
		'manzil_uz' => 'Manzil',
		'manzil_ru' => 'Aдрес',
		'manzil_en' => 'Address',

		// ****
		'ka_uz' => 'Kabinet uchun login kiriting',
		'ka_en' => 'Enter the login for the cabinet',
		'ka_ru' => 'Введите логин для кабинета',

		// ***
		'texts_uz' => 'Xush kelibsiz loginni esizdan chiqarmang kabinetga login orqali kira olasiz! Kirish uchun Tugmani bosish kifoya',
		'texts_ru' => 'Добро пожаловать в логин! Войти через логин! Просто нажмите на кнопку, чтобы получить к нему доступ',
		'texts_en' => 'Welcome to login! Log in via login! Just click on the button to access it',

		// ***
		'zakaz_uz' => 'Siz shaxsiy kabinetiz orqali zakaz berish imkoniyatingiz mavjud bo`ladi.',
		'zakaz_en' => 'You will be able to make a gift through your personal cabinet.',
		'zakaz_ru' => 'Вы сможете сделать подарок через свой личный кабинет.',

		// ****
		'news_uz' => 'Yangi kelgan xabarlar soni',
		'news_en' => 'The number of new incoming messages',
		'news_ru' => 'Количество новых входящих сообщений',

		// ****
		'use_uz' => 'Bosh sahifa',
		'use_en' => 'Home Page',
		'use_ru' => 'Домашняя страница',


		// ****
		'buyurtma_uz' => 'Berilgan buyurtmangiz soni',
		'buyurtma_en' => 'The number of your orders',
		'buyurtma_ru' => 'Количество ваших заказов',

		// ****
		'kabinets_uz' => 'Kabinetga kirish',
		'kabinets_ru' => 'Доступ на складе',
		'kabinets_en' => 'Stock access',

		// ***
		'user_uz' => 'Xush kelibsiz ',
		'user_ru' => 'желанный ',
		'user_en' => 'Welcome ',

		// *****
		'alert_uz' => 'Siz Kabinet orqali o`z kabinetizga tashrif buyura olasiz!\n Agar registratsiyadan o`tmagan bo`lsangiz o`tish orqali!',
		'alert_ru' => 'Вы можете посетить свой офис, используя кабинет! \n NЕсли вы еще не зарегистрированы, пожалуйста, продолжайте!',
		'alert_en' => 'You can visit your office using the Cabinet! \n NIf you are not registered yet, please proceed!',

		// ****
		'war_uz' => 'Bunday login mavjud emas! Siz royxatdan otib olin',
		'war_en' => 'If you have such a login, sign up',
		'war_ru' => 'Если у вас есть такой логин, зарегистрируйтесь',

		// ****
		'bizz_uz' => "Siz ro'yxatdan o'tib olgansiz!Murojatni kabinetingiz orqali amalga oshira olasiz!",
		'bizz_ru' => 'Теперь вы зарегистрированы! Вы можете сделать это через свой кабинет!',
		'bizz_en' => 'You are now registered! You can do this through your cabinet!',

		// *****
		'zakaz_uz' => 'Buyurtma berish',
		'zakaz_en' => 'Ordering',
		'zakaz_ru' => 'заказать',

		// *****
		'xabar_uz' => 'Xabaringizni qoldiring',
		'xabar_ru' => 'Оставьте свое сообщение',
		'xabar_en' => 'Leave your message',

 		// 
 		'biz_uz' => 'Biz haqimizda',
 		'biz_ru' => 'О нас',
 		'biz_en' => 'about Us',

 		// ****
 		'a_uz' => 'Biz bilan bo`ling va sifatli arzon mahsulotlarni xarid qiling. Ishonchli va sifatli xizmat biz uchun birinchi o`rinda',
 		'a_en' => 'Sell ​​us and buy good quality products. Reliable and quality service is our priority',
 		'a_ru' => 'Продайте нас и купите продукцию хорошего качества. Надежный и качественный сервис - наш приоритет',

 		// *****
 		'tarmoq_uz' => 'Ijtimoiy tarmoqlar',
 		'tarmoq_ru' => 'Социальные сети',
 		'tarmoq_en' => 'Social networks',


 		// ****
 		"mal_uz" => "Ma'lumotlar",
 		'mal_en' => 'Information',
 		'mal_ru' => 'данные',


 		// *****
 		'fir_uz' => 'Firmamiz haqida',
 		'fir_en' => 'About our company',
 		'fir_ru' => 'О нашей компании',

 		// ****
 		'xodim_uz' => 'Xodimlar haqida',
 		'xodim_ru' => 'О персонале',
 		'xodim_en' => 'About staff',

 		// *****
 		'r_uz' => 'Firma rahbari haqida',
 		'f_en' => 'About company leader',
 		'f_ru' => 'О лидере компании',


 		// *****

 		'narx_uz' => 'sum',
 		'narx_en' => "sum",
 		'narx_ru' => 'сумма',

 		// *****
 		'tug_uz' => 'Batafsil',
 		'tug_ru' => 'Узнать больше',
 		'tug_en' => "Learn more",

 		// ****
 		'ma_uz' => 'Buyurtmalar',
 		'ma_ru' => "заказы",
 		'ma_en' => "Orders",

 		// ****
 		'ma1_uz' => "Qabul qilingan",
 		'ma1_en' => 'Received',
 		'ma1_ru' => 'принят',

 		// ****
 		'all_uz' => 'Yangi buyurtmalar',
 		'all_ru' => "Все заказы",
 		'all_en' => 'All orders',

 		// ****
 		'admin_uz' => 'Adminga murojat',
 		'admin_ru' => 'Свяжитесь с вашим администратором',
 		'admin_en' => 'Contact your admin',

 		// ****
 		'foyda_uz' => 'Foydalanuvchi uchun qo`llanma',
 		'foyda_ru' => 'Руководство по использованию',
 		'foyda_en' => 'Use guide',

 		// ***
 		'title_uz' => 'Sayt izohi',
 		'title_en' => 'Site description',
 		'title_ru' => "Описание сайта",

 		// ***
 		'content_uz' => "Foydalanuvchi ro'yxatdan o'tgan xolda shaxsiya kabinet ochiladi. Shaxsiy kabinetga kirish login va fish orqali amalga oshiriladi. Ro'yxatdan o'tib olgan foydalanuvchi buyurtma berish ham soddalashadi. Asosan telefon nomer va xabar qoldirsangiz kifoya. Agar ro'yxatdan o'tmagan foydalanuvchi buyurtma berish uchun kirsa buyurtma uchun formani to'ldiradi va uni yuborish qilsa foydalanuvchi uchun yangi bir kabinet yaratiladi. Shaxsiy kabinet orqali admin bilan o'zaro aloqada bo'lish imkoniyatiga ega bo'lasiz!",
 		'content_ru' => "В случае зарегистрированного пользователя открывается личный кабинет. Гибрид сделан через логин и рыбу. Заказ зарегистрированного пользователя также упрощен. Просто отправьте номер телефона и сообщение. Если незарегистрированный пользователь вводит заказ, форма заполняет заказ и отправляет новый кабинет для пользователя. Вы сможете общаться с админом через личный кабинет!",
 		'content_en' => "In the case of a registered user personal cabinet is opened. Hybrid is made through login and fish. The ordering of the registered user is also simplified. Simply send a telephone number and message. If an unregistered user enters the order, the form fills the order and sends a new cabinet for the user. You will be able to communicate with the admin through your personal cabinet!",

 		// ****
 		'mahsulot_uz' => 'Mahsulot nomi',
 		'mahsulot_ru' => 'Название продукта',
 		'mahsulot_en' => 'Product name',

 		// ****
 		's_uz' => 'Xolat',
 		's_ru' => 'моя тетя',
 		's_en' => 'Status',

 		// ****
 		'xolat_uz' => 'Qabul qilingan',
 		'xolat_ru' => 'принят',
 		'xolat_en' => 'Received',

 		// ****
 		'xolat1_uz' => 'Qabul qilinmagan',
 		'xolat1_ru' => 'Не принято',
 		'xolat1_en' => 'Not accepted',

	 	// ****
	 	'v_uz' => 'Vaqt',
	 	'v_en' => 'Time',
	 	'v_ru' => 'время',


	 	// sardorbek va muhammadjon 



	 	// dinaraxon abbosxon
	 	// ***
	 	'btn_uz' => "Xolat tugmalari",
	 	'btn_ru' => 'Статусные ключи',
	 	'btn_en' => 'Status keys',

	 	// ***
	 	'btns_uz' => "Jo'natish",
	 	'btns_ru' => 'Oтправить',
	 	'btns_en' => 'Sending',

	 	// **
	 	'send_uz' => 'Xabar yuborildi!',
	 	'send_ru' => 'Сообщение отправлено!',
	 	'send_en' => 'Message sent!',

	 	// **
	 	'sends_uz' => 'Xabar yuborilmadi!',
	 	'sends_ru' => 'Сообщение не отправлено!',
	 	'sends_en' => 'Message not sent!',

	 	// ***
	 	'x_uz' => 'Loginni kiriting',
	 	'x_ru' => 'Введите логин',
	 	'x_en' => 'Enter the login',

	 	// ***
	 	'p_uz' => 'Parolni kiriting.',
	 	'p_ru' => 'Введите ваш пароль.',
	 	'p_en' => 'Enter your password.',

	 	// ***
	 	'u_uz' => 'Foydalanuvchilar haqida',
	 	'u_ru' => 'О пользователях',
	 	'u_en' => 'About Users',

	 	// ****
	 	'elon_uz' => 'Elonlar',
	 	'elon_ru' => 'Cообщений',
	 	'elon_en' => 'Announcements',

	 	// /*****
	 	'n2_uz' => "Yangi e'lon qo'shish",
	 	'n2_ru' => 'Добавить новое объявление',
	 	'n2_en' => "Add a new advertisement",

	 	// ****
	 	'nall_uz' => "Barcha e'lonlar",
	 	'nall_ru' => 'Все объявления',
	 	'nall_en' => 'Activities',

	 	// ****
	 	'yozishma_uz' => 'Yozishmalar',
	 	'yozishma_en' => 'Records',
	 	'yozishma_ru' => "переписка",


	 	// ****
	 	'b_uz' => 'Buyurtmalar',
	 	'b_ru' => "Заказы",
	 	'b_en' => 'Orders',

	 	// ****
	 	'bn_uz' => "Yangi buyurtmalar",
	 	'bn_ru' => "Новые заказы",
	 	'bn_en' => 'New orders',

	 	// ****
	 	'q_uz' => "Qabul qilingan buyurtmalar",
	 	'q_en' => 'Received orders',
	 	'q_ru' => "Полученные заказы",

	 	// ****
	 	'bj_uz' => "Bajarib bo'lingan buyurtmalar",
	 	'bj_ru' => 'Завершенные заказы',
	 	'bj_en' => 'Completed orders',

	 	// ****
	 	'l_uz' => "Sozlamalar",
	 	'l_ru' => "Настройки",
	 	'l_en' => 'Settings',

	 	// ***
	 	'al_uz' => 'Login va parolni almashtirish',
	 	'al_ru' => 'Изменение логина и пароля',
	 	'al_en' => "Login and Password Change",

	 	// ***
	 	'aa_uz' => 'Barcha xabarlar soni',
	 	'aa_ru' => 'Общее количество сообщений',
	 	'aa_en' => 'Total number of messages',

	 	// ***
	 	'bb_uz' => 'Buyurtmalar soni',
	 	'bb_ru' => 'Количество заказов',
	 	'bb_en' => 'Number of orders',

	 	// ***
	 	'yy_uz' => 'Yangi buyurtmalar soni',
	 	'yy_ru' => 'Новые заказы',
	 	'yy_en' => 'New orders',

	 	// ***
	 	'ff_uz' => 'Foydalanavchilar soni',
	 	'ff_ru' => 'Количество пользователей',
	 	'ff_en' => 'Number of users',

	 	// ****
	 	'sort_uz' => 'Soni boyicha ko`rish',
	 	'sort_ru' => 'Просмотр по номеру',
	 	'sort_en' => 'Viewing by number',

	 	// ****
	 	'use_login_uz' => 'Foydalanuvchi logini',
	 	'use_login_ru' => 'Логин пользователя',
	 	'use_login_en' => 'User login',
	

	 	// *****
	 	'imkon_uz' => "Qo'shimcha imkoniyatlar",
	 	'imkon_ru' => 'Дополнительные опции',
	 	'imkon_en' => 'Additional options',

	 	// ***
	 	'del_uz' => "O'chirish",
	 	'del_ru' => 'Удалить',
	 	'del_en' => "Delete",

	 	// ***
	 	'bog_uz' => "Bog'lanish",
	 	'bog_ru' => 'Связаться с Нами',
	 	'bog_en' => 'Contact',

	 	// *****
	 	'mah_uz' => "Mahsulot nomini kiriting uzbekcha ma'lumot",
	 	'mah_ru' => 'Введите название продукта Узбекская информация',
	 	'mah_en' => "Enter product name Uzbek information",

	 	// ***
	 	'mah1_uz' => "Mahsulot nomini kiriting ruscha ma'lumot",
	 	'mah1_ru' => 'Введите название продукта русский информация',
	 	'mah1_en' => "Enter product name Russian information",

	 	// ***

	 	// ***
	 	'mah2_uz' => "Mahsulot nomini kiriting ingiliz tilida ma'lumot",
	 	'mah2_ru' => 'Введите название продукта на английском информация',
	 	'mah2_en' => "Enter product name in english information",

	 	// ***
	 	'text_uz' => "Mahsulot haqida batafsil ma'lumot uzbek tilida kiriting",
	 	'text_ru' => 'Введите подробную информацию о товаре на узбекском',
	 	'text_en' => "Enter detailed information about the product in Uzbek",

	 	// ***
	 	'text1_uz' => "Mahsulot haqida batafsil ma'lumot rus tilida kiriting",
	 	'text1_ru' => 'Для получения дополнительной информации о продукте, пожалуйста, пишите на русском языке',
	 	'text1_en' => "For more information about the product, please write in Russian",

	 	// ***
	 	'text2_uz' => "Mahsulot haqida batafsil ma'lumot ingiliz tilida kiriting",
	 	'text2_ru' => 'Для получения подробной информации о продукте, пожалуйста, введите на английском языке',
	 	'text2_en' => "For detailed product information please enter in English",

	 	// ***
	 	'rasm_uz' => 'Mahsulot rasmini biriktiring',
	 	'rasm_en' => 'Paste the product image',
	 	'rasm_ru' => 'Вставьте изображение продукта',


	 	// ****
	 	'su_uz' => "Mahsulot summasini kiriting",
	 	'su_en' => 'Enter product amount',
	 	'su_ru' => 'Введите сумму продукта',

	 	// ****
	 	'bol_uz' => "Bo'limni biriktiring",
	 	'bol_ru' => "Пожалуйста, прикрепите раздел",
	 	'bol_en' => 'Please attach the section',


	 	// ****
	 	'cheg_uz' => "Chegirmani kiriting",
	 	'cheg_en' => 'Please enter a receipt',
	 	'cheg_ru' => 'Пожалуйста, введите квитанцию',


	 	// ****
	 	'save_uz' => 'Saqlash',
	 	'save_ru' => 'удержание',
	 	'save_en' => "Save",


	 	// ****
	 	'mm_uz' => 'Mahsulot nomi',
	 	'mm_ru' => 'Название продукта',
	 	'mm_en' => 'Product name',

	 	// ****
	 	'i_uz' => 'Mahsulot haqida batafsil',
	 	'i_ru' => 'Подробнее о продукте',
	 	'i_en' => 'More about the product',

	 	// ****
	 	'r_uz' => "Mahsulot rasm",
	 	'r_en' => 'Product picture',
	 	'r_ru' => 'Изображение продукта',


	 	// ****
	 	'sum_uz' => 'Mahsulot narx',
	 	'sum_ru' => 'Цена продукта',
	 	'sum_en' => 'Product Price',

	 	// ****
	 	'n_uz' => "Bo'lim nomi",
	 	'n_ru' => "Название отдела",
	 	'n_en' => 'Department name',

	 	// ****
	 	'ch_uz' => 'Mahsulot chegirmasi',
	 	'ch_ru' => 'Скидка продукта',
	 	'ch_en' => 'Product Discount',


	 	// ****
	 	'k_uz' => 'Kiritilgan vaqti',
	 	'k_ru' => 'Дата добавления',
	 	'k_en' => 'Date added',

	 	// ****
	 	'uuuu_uz' => "Yuborilgan xabarlar",
	 	'uuuu_en' => "Sent messages",
	 	'uuuu_ru' => "Отправленные сообщения",

	 	// ****

	 	'qabul_uz' => "Qabul qilingan xabarlar",
	 	'qabul_en' => "Received messages",
	 	'qabul_ru' => "Полученные сообщения",

	 	// ****
	 	'sen_uz' => "Buyurtma berish uchun siz <strong>Bosh sahifa</strong> oynasiga chiqib olishingiz kerek. Buyurtma berishingiz mumkin bo'ladi. Kabinet siz va admin orasidagi muloqatni ta'minlab beradi. Bosh sahifa orqali asosiy oynaga chiqib olib buyurtma bersangiz bo'ladi. Formaga fish,telefon nomer, xabar, va to'g'ri kapcha kiritsangiz kifoya!",
	 	'sen_en' => 'To order, you need to get to the <strong> Home Page </ strong> window. You can order. The Cabinet maintains a dialogue between you and the admin. You can place your order on the home page and order it. Just fill in the form with a fish, a phone number, a message, and a correct hinge!',
	 	'sen_ru' => 'Чтобы оформить заказ, вам нужно перейти в окно <strong> домашняя страница </ strong>. Вы можете заказать. Кабинет поддерживает диалог между вами и администратором. Вы можете разместить свой заказ на главной странице и заказать его. Просто заполните форму с рыбой, номером телефона, сообщением и правильной петлей!',


	 	// ***
	 	'qqq_uz' => 'xabar',
	 	'qqq_en' => 'message',
	 	'qqq_ru' => 'доклад',

	 	// ****
	 	't_uz' => 'Telefon nomer',
	 	't_ru' => 'Номер телефона',
	 	't_en' => 'Telephone number',

	 	// ****
	 	'va_kel_uz' => 'Kelgan vaqti',
	 	'va_kel_en' => "It's time to come",
	 	'va_kel_ru' => 'Пришло время',


	 	// ***
	 	'date_uz' => 'Qabul qilingan vaqt',
	 	'date_ru' => 'Время заняло',
	 	'date_en' => "Time taken",

	 	// ****
	 	'rad_uz' => 'Qabul qilinmagan',
	 	'rad_ru' => "Не принято",
	 	'rad_en' => 'Not accepted',

	 	// ***
	 	'mmmm_uz' => 'Bizning manzil',
	 	'mmmm_ru' => 'Наш адрес',
	 	'mmmm_en' => 'Our address',

	 	// ***
	 	'ml_uz' => "Farg'ona shahar Oltiariq viloyati",
	 	'ml_ru' => "Город Фергана Алтыарыкский район",
	 	'ml_en' => 'Fergana city is Altyarik area',


	 	// ***
	 	"vvv_uz" => "Biz bilan bog'lanish uchun",
	 	'vvv_ru' => "Связаться с нами",
	 	'vvv_en' => "Contact Us",

	 	// ****
	 	'mes_uz' => "Siz ro'yxatdan o'tib olgansiz!\nShu tufayli buyurtma berish uchun quydagi formani to'ldirishingizni o'zi kifoya!",
	 	'mes_ru' => 'Вы были зачислены! \ NTo заполните это, просто заполните форму ниже!',
	 	'mes_en' => "You've been enrolled! \ NTo fill this out, simply fill out the form below!",

	 	// ***
 	];

	
?>