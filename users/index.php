<?php 
  session_start();
  include('../config.php');
  include('../lib/db.php');
  include('../lib/admin.php');
  include('../til/til.php');
  
  $til = $_SESSION['til'] ?? 'uz';
  $view = $_GET['view'] ?? 'index';
  if(isAuth()){
    include($config['base']['path']."views/users_layouts/main.php");
  }
  else{
    include($config['base']['path']."views/users_pages/login.php");
  }
?>