<?php 
    define('URL', 'http://localhost/textil_prect/admin/');
    define('urlm', 'http://localhost/textil_prect/');
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin </title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=$config['base']['url']."web/users_css/"?>css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?=$config['base']['url']."web/users_css/"?>css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=$config['base']['url']."web/users_css/"?>css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=$config['base']['url']."web/users_css/"?>font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=URL?>"><?=$_SESSION['login']?></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php echo $array['news_'.$til]; ?> <span class="badge badge-info"><?=getZakaz('xabar', 'status', 'active');?></span>
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages" style="background: black">
                        <?php foreach(getOneInfo('xabar', 'status', 'active') as $r): ?>
                            <li style="background: orange">
                                <a href="<?=URL.'one/'.$r['id']?>">
                                    <div>
                                        <strong></strong>
                                        <span class="pull-right text-muted">
                                            <em><?=$r['vaqt']?></em>
                                        </span>
                                    </div>
                                    <div><?=substr($r['xabar'], 0, 30);?></div>
                                </a>
                            </li>
                             <li class="divider" style="background: red;"></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <li class="">
                    <a href="<?=urlm;?>">
                        <?=$array['use_'.$til];?>
                        <i class="fa fa-desktop fa-fw"></i>
                    </a>
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a href="<?=URL;?>logout/2">
                        <?=$array['chiqish_'.$til];?>
                        <i class="fa fa-user fa-fw"></i>
                    </a>
                </li>
                <!-- /.dropdown -->
            </ul>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?=URL.'user/1'?>"><i class="fa fa-dashboard fa-fw"></i> <?=$array['u_'.$til];?></a>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-bar-chart-o fa-fw"></i> <?=$array['elon_'.$til]?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?=URL.'news/1'?>"><?=$array['n2_'.$til];?></a>
                                </li>
                                <li>
                                    <a href="<?=URL.'all/1'?>"><?=$array['nall_'.$til];?></a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> <?=$array['b_'.$til];?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?=URL.'buyurtma/1'?>"><?=$array['bn_'.$til];?></a>
                                </li>
                                <li>
                                    <a href="<?=URL.'qabul/1'?>"><?=$array['q_'.$til];?></a>
                                </li>
                                <li>
                                    <a href="<?=URL."bajarib/1"?>"><?=$array['bj_'.$til];?></a>
                                </li>
                            </ul>
                        </li>
                       
                        <li class="active">
                            <a href=""><i class="fa fa-wrench fa-fw"></i><?=$array['l_'.$til];?><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="active" href="<?=URL.'almashtir/1'?>"><?=$array['al_'.$til]?></a>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <?php include($config['base']['path']."views/admin_pages/".$view.".php"); ?>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?=$config['base']['url']."web/users_css/"?>js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=$config['base']['url']."web/users_css/"?>js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=$config['base']['url']."web/users_css/"?>js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?=$config['base']['url']."web/users_css/"?>js/sb-admin-2.js"></script>

</body>

</html>
