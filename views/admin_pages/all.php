<div id="page-wrapper">
    <div class="row">

    	<form action="" method="post">
            <?=$array['sort_'.$til];?>
            <select name="son" class="form-control" id="son">
                <option value="5"><?=$array['sort_'.$til];?></option>
                <option>10</option>
                <option>25</option>
                <option>45</option>
                <option>55</option>
                <option>100</option>
                <option>Barchasi</option>
            </select>
        </form>
        <a href="<?=URL.'/news/1'?>" class="btn btn-success">
        	<i class="glyphicon glyphicon-plus"></i>
        </a>
		<table class="table table-dark">
			<thead style="background: black; color: white; width: 100%; box-shadow: 0 0 5px orange">
				<hr>
				<th style="text-align: center; border-left: 2px solid white; font-size: 11px; font-weight: bold"><?=$array['mm_'.$til]?></th>
				<th style="text-align: center; border-left: 2px solid white; font-size: 11px; font-weight: bold"><?=$array['i_'.$til];?></th>
				<th style="text-align: center; border-left: 2px solid white; font-size: 11px; font-weight: bold"><?=$array['r_'.$til];?></th>
				<th style="text-align: center; border-left: 2px solid white; font-size: 11px; font-weight: bold"><?=$array['r_'.$til];?></th>
				<th style="text-align: center; border-left: 2px solid white; font-size: 11px; font-weight: bold"><?=$array['sum_'.$til];?></th>
				<th style="text-align: center; border-left: 2px solid white; font-size: 11px; font-weight: bold"><?=$array['n_'.$til];?></th>
				<th style="text-align: center; border-left: 2px solid white; font-size: 11px; font-weight: bold"><?=$array['ch_'.$til];?></th>
				<th style="text-align: center; border-left: 2px solid white; font-size: 11px; font-weight: bold"><?=$array['k_'.$til];?></th>
				<th style="text-align: center; border-left: 2px solid white; font-size: 11px; font-weight: bold"><?=$array['imkon_'.$til]?></th>
			</thead>
			<tbody class="ajax">
				<?php foreach(getsLimits('mahsulotlar', 10) as $r): ?>
					<tr>
						<td align="center"><?=$r['nomi_'.$til]?></td>
						<td align="center"><?=substr($r['izoh_'.$til], 0, 75);?></td>
						<td align="center" width="120px;" height="100px;"><?="<img src='".$config['base']['url'].'web/images/'.$r['rasm1']."' width='100%'>"?></td>
						<td align="center" width="120px;" height="100px;"><?="<img src='".$config['base']['url'].'web/images/'.$r['rasm2']."' width='100%'>"?></td>
						<td align="center"><?=$r['narx']?></td>
						<td align="center"><?=$r['subbolim_nomi']?></td>
						<td align="center"><?=$r['chegirma']?></td>
						<td align="center"><?=$r['vaqt']?></td>
						<td width="130px;">
							<a href="<?=URL?>aloqa/<?=$r['id']?>" class="btn btn-info btn-sm" title='Batafsil'>
								<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</a>
							<a href="<?=URL.'del/'.$r['id']?>" class="btn btn-warning btn-sm">
								<i class="fa fa-trash-o" aria-hidden="true"></i>
							</a>
							<a href="<?=URL.'batafsil/'.$r['id']?>" class="btn btn-success btn-sm">
								<i class="fa fa-eye" aria-hidden="true"></i>
							</a>
						</td>
					</tr>

				<?php endforeach; ?>

			</tbody>
		</table>
    </div>
    <hr>
</div>
<script src="<?=$config['base']['url']."web/js/jQuery.js"?>"></script>
<script type="text/javascript">
    $(function(){
        $('#son').change(function(){
            $.ajax({
                url: "<?=$config['base']['url']."views/admin_pages/tests.php"?>",
                type: 'POST',
                data: {'name': $('#son').val(), 'nomi': 'mahsulotlar'},
                success: function(e){
                    $('.ajax').html(e);
                }
            })
        })
    })
</script>