<div id="page-wrapper">
    <div class="row">
		<?php foreach(getOneInfo('zakaz', 'id', $_GET['til']) as $r): ?>
			<div class="col-sm-12" style="background: orange">
				<h4>
					<i class="fa fa-user" aria-hidden="true"></i>
					<?=$r['fish']?>
				</h4>
				<p>
					<i class="fa fa-database" aria-hidden="true"></i>
					<strong><?=$r['mahsulot_nomi']?></strong>
				</p>
				<h5>
					<i class="fa fa-home" aria-hidden="true"></i>
					<big><?=$r['manzil']?></big>
				</h5>
				<p>
					<i class="fa fa-phone-square" aria-hidden="true"></i>
					<strong><?=$r['tel']?></strong>
				</p>
				<p>
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					<?=$r['izoh']?>
				</p>
				<div>
					<a href="<?=URL.'qabul/'.$_GET['til']?>" class="btn btn-info btn-sm">Qabul qilish</a>
					<a href="<?=URL.'rad/'.$_GET['til']?>" class="btn btn-danger btn-sm">Rad qilish</a>
				</div>
				<hr>
			</div>
		<?php endforeach; ?>
    </div>
</div>