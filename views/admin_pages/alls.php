<div id="page-wrapper">
    <div class="row">
		<div class="col-sm-12">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?=AllCounts('zakaz');?></div>
                            <div><?=$array['yy_'.$til];?>!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
        	<span style="background: orange; color: white; padding: 10px;">Qabul qilingan</span>
        	<span style="background: blue; color: white; padding: 10px;">Rad etilgan</span>
        	<span style="background: black; color: white; padding: 10px;">Yangi buyurtma</span>
        	<span style="background: red; color: white; padding: 10px;">Yakunlangan</span>
        	<table width="100%" class="table table-bordered" style="background: #ccc; color: black">
        		<thead>
        			<th>FISH</th><th>Mahsulot nomi</th><th>Izoh</th><th>Telefon nomer</th><th>Manzili</th><th>Kelgan vaqti</th><th>Login</th>
        		</thead>
        	<?php foreach(getAll('zakaz') as $r): ?>
				<tr style="<?php if($r['status'] == 'qabul'){ 
						echo 'background: orange'.";";}
					elseif($r['status'] == 'tamom'){
						echo 'background: red'.";";}
					elseif($r['status'] == 'active'){
						echo 'background: black'.';';
					}
					elseif($r['status'] == 'rad_etish'){
						echo "background: blue".';';
					}
					?> color: white; text-align: center;">
					<td><i class="fa fa-user" aria-hidden="true"></i><br> <?=$r['fish']?></td>
					<td><i class="fa fa-database" aria-hidden="true"></i><br> <?=$r['mahsulot_nomi']?></td>
					<td>
						<i class="fa fa-comments" aria-hidden="true"></i><br>
						<p><?=substr($r['izoh'], 0, 50).'..'?></p>
					</td>
					<td>
						<i class="fa fa-phone-square" aria-hidden="true"></i><br>
						<strong><?=$r['tel']?></strong>
					</td>
					<td>
						<i class="fa fa-home" aria-hidden="true"></i><br>
						<p><?=$r['manzil']?></p>
					</td>
					<td>
						<i class="fa fa-calendar" aria-hidden="true"></i><br>
						<data><?=$r['vaqt']?></data>
					</td>
					<td>
						<i class="fa fa-sign-in" aria-hidden="true"></i><br>
						<strong><?=$r['login']?></strong>
					</td>
					
				</tr>
        	<?php endforeach; ?>
        	</table>
        </div>
    </div>
</div>