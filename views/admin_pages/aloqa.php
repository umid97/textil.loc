<div id="page-wrapper">
    <div class="row">
		<?php if(isset($_GET['view'])): ?>
			<?php foreach(getOneInfo('mahsulotlar', 'id', $_GET['til']) as $r): ?>
				<form action="<?=URL."update/1"?>" method="post" enctype='multipart/form-data'>
				<hr>
				<label>
					<strong><?=$array['mah_'.$til]?></strong>
					<input required type="text" class="form-control" size="110" name="name_uz" value="<?=$r['nomi_uz']?>">
				</label>
				<hr>
				<label>
					<strong><?=$array['mah1_'.$til];?></strong>
					<input required type="text" class="form-control" size="110" name="name_ru" value="<?=$r['nomi_ru']?>">
				</label>
				<hr>
				<label>
					<strong><?=$array['mah2_'.$til];?></strong>
					<input required type="text" class="form-control" size="110" name="name_en" value="<?=$r['nomi_en']?>">
				</label>
				<hr>
				<label>
					<strong><?=$array['text_'.$til];?></strong>
					<textarea required name="text_uz" id="" cols="111" rows="5" class="form-control"><?=$r['izoh_uz']?></textarea>
				</label>
				<hr>
				<label>
					<strong><?=$array['text1_'.$til];?></strong>
					<textarea required name="text_ru" id="" cols="111" rows="5" class="form-control"><?=$r['izoh_ru']?></textarea>
				</label>
				<hr>
				<label>
					<strong><?=$array['text2_'.$til];?></strong>
					<textarea required name="text_en" id="" cols="111" rows="5" class="form-control"><?=$r['izoh_en']?></textarea>
				</label>
				<hr>
				<label>
					<strong>1 <?=$array['rasm_'.$til];?></strong>
					<input type="file" name="rasm1" class="form-control">
				</label>
				<label>
					<strong>2 <?=$array['rasm_'.$til];?></strong>
					<input type="file" name="rasm2" class="form-control">
				</label>
				<label>
					<strong><?=$array['bol_'.$til];?></strong>
					<select  name="bolim" id="" class="form-control form-control-lg">
						<?php foreach(getAll('subbolims') as $row): ?>
							<option <?php if($r['subbolim_nomi'] == $row['nomi_uz']){echo ' selected'; }?>><?=$row['nomi_'.$til]?></option>
						<?php endforeach; ?>
					</select>
				</label>
				<hr>
				<input type="hidden" name="id" value="<?=$_GET['til']?>">
				<input type="hidden" name="img1" value="<?=$r['rasm1']?>">
				<input type="hidden" name="img2" value="<?=$r['rasm2']?>">
				<label>
					<strong>2 <?=$array['su_'.$til];?></strong>
					<input required type="text" name="narx" size="110" class="form-control" value="<?=$r['narx']?>">
				</label>
				<hr>
				<label>
					<strong><?=$array['cheg_'.$til];?></strong>
					<input required type="text" name="chegirma" size="110" class="form-control" value="<?=$r['chegirma']?>">
				</label>
				<hr>
				<input type="submit" name="ok" class="form-control btn btn-success" value="<?=$array['save_'.$til];?>"><hr>

			</form>
			<?php endforeach; ?>
		<?php endif; ?>
    </div>
</div>