<div id="page-wrapper">
    <div class="row">
		<div class="col-sm-12">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?=getZakaz('zakaz', 'status', 'tamom');?></div>
                            <div>Tamomlangan buyurtmalar</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
        	<table width="100%" class="table table-bordered" style="background: #ccc; color: black">
        		<thead>
        			<th>FISH</th><th>Mahsulot nomi</th><th>Izoh</th><th>Telefon nomer</th><th>Manzili</th><th>Kelgan vaqti</th><th>Login</th><th>Bajarilgan vaqt</th><th>Tugmalar</th>
        		</thead>
        	<?php foreach(GetWhere('zakaz', 'status', 'tamom') as $r): ?>
				<tr style="background-color: #9C2A2A; color: white; text-align: center;">
					<td><i class="fa fa-user" aria-hidden="true"></i><br> <?=$r['fish']?></td>
					<td><i class="fa fa-database" aria-hidden="true"></i><br> <?=$r['mahsulot_nomi']?></td>
					<td>
						<i class="fa fa-comments" aria-hidden="true"></i><br>
						<p><?=substr($r['izoh'], 0, 50).'..'?></p>
					</td>
					<td>
						<i class="fa fa-phone-square" aria-hidden="true"></i><br>
						<strong><?=$r['tel']?></strong>
					</td>
					<td>
						<i class="fa fa-home" aria-hidden="true"></i><br>
						<p><?=$r['manzil']?></p>
					</td>
					<td>
						<i class="fa fa-calendar" aria-hidden="true"></i><br>
						<data><?=$r['vaqt']?></data>
					</td>
					<td>
						<i class="fa fa-sign-in" aria-hidden="true"></i><br>
						<strong><?=$r['login']?></strong>
					</td>
					<td>
						<i class="fa fa-clock-o" aria-hidden="true"></i>
						<data><?=$r['tamom_vaqt']?></data>
					</td>
					<td>
						<a href="<?=URL.'delete/1'?>" title="O'chirish" class="btn btn-warning btn-sm">
							<i class="fa fa-trash-o" aria-hidden="true"></i>
						</a>
					</td>
				</tr>
        	<?php endforeach; ?>
        	</table>
        </div>

    </div>
</div>