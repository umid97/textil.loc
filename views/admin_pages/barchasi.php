 <div id="page-wrapper">
    <div class="row" style="margin-top: 10px;">
    	<table width="100%" class="table table-bordered" style="background: #ccc; color: black">
    		Sizga kelgan xabarlar
    		<thead>
    			<th>FISH</th><th>Xabar</th><th>Kelgan vaqt</th><th>Qo'shimcha imkoniyatlar</th>
    		</thead>
    		<tbody style="background: orange">
				<?php foreach(getAll('xabar') as $r): ?>
					<tr>
						<td>
							<?=$r['fish']?>
						</td>
						<td>
							<?=$r['xabar'];?>
						</td>
						<td>
							<?=$r['vaqt']?>
						</td>
						<td>
							<a href="<?=URL.'ochirish/'.$r['id']?>" class='btn btn-danger'>O'chirish</a>
							<a href="<?=URL.'ones/'.$r['id']?>" class='btn btn-success'>Batafsil</a>
						</td>
					</tr>
				<?php endforeach; ?>
		</tbody>
    </div>
</div>