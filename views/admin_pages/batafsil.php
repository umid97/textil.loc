<div id="page-wrapper">
    <div class="row">
    	<?php foreach(getOneInfo('mahsulotlar', 'id', $_GET['til']) as $r): ?>
		<div class="col-sm-6">
			<div class="thumbnail">
				<img style="width: 100%; height: 300px;" src="<?=$config['base']['url']."web/images/".$r['rasm1']?>" alt="">
				<?php if($r['chegirma'] > 0): ?>
					<span class="btn btn-success btn-sm"><?=$r['chegirma']?>%</span>
				<?php endif; ?>
				<?php if($r['status'] == 'active'): ?>
					<span class="btn btn-success btn-sm">
						Faol
						<i class="fa fa-check" aria-hidden="true"></i>
					</span>
				<?php elseif($r['status'] == 'noactive'): ?>
					<span class="btn btn-success btn-sm">
						Faol emas
						<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
					</span>
				<?php endif; ?>

			</div>
		</div>
		<div class="col-sm-6">
			<div class="thumbnail">
				<img style="width: 100%; height: 300px;" src="<?=$config['base']['url']."web/images/".$r['rasm2']?>" alt="">
				<?php if($r['chegirma'] > 0): ?>
					<span class="btn btn-success btn-sm"><?=$r['chegirma']?>%</span>
				<?php endif; ?>
				<?php if($r['status'] == 'active'): ?>
					<span class="btn btn-success btn-sm">
						Faol
						<i class="fa fa-check" aria-hidden="true"></i>
					</span>
				<?php elseif($r['status'] == 'noactive'): ?>
					<span class="btn btn-success btn-sm">
						Faol emas
						<i class="fa fa-exclamation-circle" aria-hidden="true"></i>
					</span>
				<?php endif; ?>

				
			</div>
		</div>
		<div class="col-sm-12">
			<h4><i class="fa fa-header btn btn-success" aria-hidden="true" ></i> <?=$r['nomi_'.$til]?></h4>
			<p align="justify"><i class="fa fa-align-justify btn btn-success" aria-hidden="true"></i>  <?=$r['izoh_'.$til]?></p>
			<h5><i class="fa fa-puzzle-piece btn btn-success" aria-hidden="true"></i> Bo'lim nomi - <strong><?=$r['subbolim_nomi']?></strong></h5>
			<strong><i class="fa fa-money btn btn-success " aria-hidden="true"></i> Narx - <?=$r['narx']?></strong><hr>
			<p><i class="fa fa-calendar btn btn-success" aria-hidden="true"> Kiritilgan sana  - <?php echo $r['vaqt']; ?></i></p>
		</div>
		<?php endforeach; ?>
    </div>
</div>