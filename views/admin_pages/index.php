 <div id="page-wrapper">
    <div class="row" style="margin-top: 10px;">
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-comments fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?=AllCount('xabar')?></div>
                            <div>Barcha xabarlar</div>
                        </div>
                    </div>
                </div>
                <a href="<?=URL.'barchasi/1'?>">
                    <div class="panel-footer">
                        <span class="pull-left">Batafsil</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?=AllCount('zakaz')?></div>
                            <div><?=$array['bb_'.$til]?>!</div>
                        </div>
                    </div>
                </div>
                <a href="<?=URL.'alls/1'?>">
                    <div class="panel-footer">
                        <span class="pull-left">Batafsil</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?=AllCounts('zakaz', 'active')?></div>
                            <div><?=$array['yy_'.$til];?>!</div>
                        </div>
                    </div>
                </div>
                <a href="<?=URL.'buyurtma/1'?>">
                    <div class="panel-footer">
                        <span class="pull-left">Batafsil</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-support fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?=AllCount('contact');?></div>
                            <div><?=$array['ff_'.$til]?>!</div>
                        </div>
                    </div>
                </div>
                <a href="<?=URL.'user/1'?>">
                    <div class="panel-footer">
                        <span class="pull-left">Batafsil</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    
</div>