<div id="page-wrapper">
    <div class="row">
    	<form action="" method="post">
    		<label>
    			<strong>Yangi loginni kiriting</strong>
    			<input type="text" id="log" name="login" class="form-control" required>
    		</label>
    		<label>
    			<strong>Yangi parolni kiriting</strong>
    			<input type="password" id="par" name="parol" class="form-control" required>
    		</label>
    		<input type="submit" name="ok" value="Yangilash" class='btn btn-success'>
    	</form>
    <?php 
        if(isset($_POST['ok'])){
            if(updateParol($_POST['login'], $_POST['parol'])){
                logout();
                ?>
                    <script type="text/javascript">
                        window.location = '../';
                    </script>
                <?php

            }
            else{
                ?>
                    <h3>Almashtirib bo'lmadi</h3>
                <?php
            }
        }
        else{

        } 

    ?>
    </div>
</div>

