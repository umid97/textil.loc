<?php 
define('URL', 'http://localhost/textil_prect/admin/');
include('../../config.php');
include('../../lib/db.php');
include('../../til/til.php');
$til = $_SESSION['til'] ?? 'uz';
?>
<?php if($_POST['name'] == 'Barchasi'): ?>
<?php foreach(getAllMahsulot('mahsulotlar') as $r): ?>
	<tr>
		<td><?=$r['nomi_'.$til]?></td>
		<td><?=substr($r['izoh_'.$til], 0, 50)?></td>
		<td align="center" width="120px;" height="100px;"><?="<img src='".$config['base']['url'].'web/images/'.$r['rasm1']."' width='100%'>"?></td>
		<td align="center" width="120px;" height="100px;"><?="<img src='".$config['base']['url'].'web/images/'.$r['rasm2']."' width='100%'>"?></td>
		<td><?=$r['narx']?></td>
		<td><?=$r['subbolim_nomi']?></td>
		<td><?=$r['chegirma']?></td>
		<td><?=$r['vaqt']?></td>
		<td width="130px;">
			<a href="<?=URL?>aloqa/<?=$r['id']?>" class="btn btn-info btn-sm" title='Batafsil'>
				<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
			</a>
			<a href="<?=URL.'del/'.$r['id']?>" class="btn btn-warning btn-sm">
				<i class="fa fa-trash-o" aria-hidden="true"></i>
			</a>
			<a href="<?=URL.'batafsil/'.$r['id']?>" class="btn btn-success btn-sm">
				<i class="fa fa-eye" aria-hidden="true"></i>
			</a>
		</td>
	</tr>
<?php endforeach; ?>
<?php else: ?>
<?php foreach(getsLimits($_REQUEST['nomi'],$_REQUEST['name']) as $r): ?>
	<tr>
		<td><?=$r['nomi_'.$til]?></td>
		<td><?=substr($r['izoh_'.$til], 0, 50)?></td>
		<td align="center" width="120px;" height="100px;"><?="<img src='".$config['base']['url'].'web/images/'.$r['rasm1']."' width='100%'>"?></td>
		<td align="center" width="120px;" height="100px;"><?="<img src='".$config['base']['url'].'web/images/'.$r['rasm2']."' width='100%'>"?></td>
		<td><?=$r['narx']?></td>
		<td><?=$r['subbolim_nomi']?></td>
		<td><?=$r['chegirma']?></td>
		<td><?=$r['vaqt']?></td>
		<td width="130px;">
			<a href="<?=URL?>aloqa/<?=$r['id']?>" class="btn btn-info btn-sm" title='Batafsil'>
				<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
			</a>
			<a href="<?=URL.'del/'.$r['id']?>" class="btn btn-warning btn-sm">
				<i class="fa fa-trash-o" aria-hidden="true"></i>
			</a>
			<a href="<?=URL.'batafsil/'.$r['id']?>" class="btn btn-success btn-sm">
				<i class="fa fa-eye" aria-hidden="true"></i>
			</a>
		</td>
		<td></td>
	</tr>
<?php endforeach; ?>
<?php endif; ?>