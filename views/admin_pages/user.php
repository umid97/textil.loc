<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <table class="table bordered table-hover table-dark" width="100%">
                <thead>
                    <tr>
                        <th><?=$array['fish_'.$til];?></th>
                        <th><?=$array['tel_'.$til];?></th>
                        <th><?=$array['manzil_'.$til];?></th>
                        <th><?=$array['use_login_'.$til];?></th>
                        <th><?=$array['imkon_'.$til]?></th>
                        <th>
                            <form action="" method="post">
                                <?=$array['sort_'.$til];?>
                                <select name="son" class="form-control" id="son">
                                    <option value="5"><?=$array['sort_'.$til];?></option>
                                    <option>10</option>
                                    <option>25</option>
                                </select>
                            </form>
                        </th>
                    </tr>
                </thead>
                <tbody class="ajax">
                    <?php foreach(getsLimits('contact',5) as $r): ?>
                        <tr>
                            <td><?=$r['fish']?></td>
                            <td><?=$r['tel']?></td>
                            <td><?=$r['manzil']?></td>
                            <td><?=$r['login']?></td>
                            <td>
                                <a href="<?=URL?>connect/<?=$r['id']?>" class="btn btn-info btn-sm"><?=$array['bog_'.$til];?></a>
                                <a href="<?=URL?>d/<?=$r['id']?>" class="btn btn-warning btn-sm"><?=$array['del_'.$til];?></a>
                            </td>
                            <td></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>

<script src="<?=$config['base']['url']."web/js/jQuery.js"?>"></script>
<script type="text/javascript">
    $(function(){
        $('#son').change(function(){
            $.ajax({
                url: "<?=$config['base']['url']."views/admin_pages/test.php"?>",
                type: 'POST',
                data: {'name': $('#son').val(), 'nomi': 'contact'},
                success: function(e){
                    $('.ajax').html(e);
                }
            })
        })
    })
</script>