<?php 
    define('URLMANAGER', 'http://localhost/textil_prect/'.$view);
    session_start(); 
        
    if(isset($_GET['til']) && $_GET['til'] == 'uz'){
        $_SESSION['til'] = $_GET['til'];
    }
    elseif(isset($_GET['til']) && $_GET['til'] == 'ru'){
        $_SESSION['til'] = $_GET['til'];
    }
    elseif(isset($_GET['til']) && $_GET['til'] == 'en'){
        $_SESSION['til'] = $_GET['til'];
    }
    
   

    $til = $_SESSION['til'] ?? 'uz';
    

 ?>

<!DOCTYPE html>
<html dir="ltr" lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Textil mchj</title>
    <base />
    <meta name="description" content="My Store" />
    
    
    <script src="<?=$config['base']['url']."web/"?>catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="<?=$config['base']['url']."web/"?>catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="<?=$config['base']['url']."web/"?>catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,400i,300,500,600,700" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:500,700,900,300,400' rel='stylesheet' type='text/css'>
    <link href="<?=$config['base']['url']."web/"?>catalog/view/theme/OPC090216_1/stylesheet/stylesheet.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=ABeeZee:400,400i" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']."web/"?>catalog/view/theme/OPC090216_1/stylesheet/megnor/carousel.css" />
    <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']."web/"?>catalog/view/theme/OPC090216_1/stylesheet/megnor/custom.css" />
    <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']."web/"?>catalog/view/theme/OPC090216_1/stylesheet/megnor/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']."web/"?>catalog/view/theme/OPC090216_1/stylesheet/megnor/lightbox.css" />
    <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']."web/"?>catalog/view/theme/OPC090216_1/stylesheet/megnor/animate.css" />
    <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']."web/"?>catalog/view/theme/OPC090216_1/stylesheet/megnor/search_suggestion.css" />
    <link rel="stylesheet" type="text/css" href="<?=$config['base']['url']."web/"?>catalog/view/javascript/jquery/magnific/magnific-popup.css" />

    <link href="<?=$config['base']['url']."web/"?>catalog/view/javascript/jquery/ui/jquery-ui.css" type="text/css" rel="stylesheet" media="screen" />
    <link href="<?=$config['base']['url']."web/"?>catalog/view/javascript/jquery/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen" />
    <link href="<?=$config['base']['url']."web/"?>catalog/view/javascript/jquery/owl-carousel/owl.transitions.css" type="text/css" rel="stylesheet" media="screen" />

    <!-- Megnor www.templatemela.com - Start -->
   
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/megnor/custom.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/megnor/jstree.min.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/megnor/carousel.min.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/megnor/megnor.min.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/megnor/jquery.custom.min.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/megnor/jquery.formalize.min.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/lightbox/lightbox-2.6.min.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/megnor/tabs.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/megnor/jquery.elevatezoom.min.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/megnor/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/megnor/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/megnor/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/megnor/doubletaptogo.js"></script>
    <script type="text/javascript" src="<?=$config['base']['url']."web/"?>catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>
    <script src="<?=$config['base']['url']."web/"?>catalog/view/javascript/common.js" type="text/javascript"></script>
    <link href="<?=$config['base']['url']."web/"?>image/catalog/cart.png" rel="icon" />
    <script src="<?=$config['base']['url']."web/"?>catalog/view/javascript/search_suggestion/search_suggestion.js" type="text/javascript"></script>
    <script src="<?=$config['base']['url']."web/"?>catalog/view/javascript/jquery/ui/jquery-ui.js" type="text/javascript"></script>
    <script src="<?=$config['base']['url']."web/"?>catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
    <style>
        #til li{
            display: inline-block;

        }
        #til li a{
            background: #E87E00;
            color: white;
            padding: 5px 20px;
            box-shadow: 0px 2px 10px white;
        }
        #til li a:hover{

            background-image: linear-gradient(#E87E00, #F75708 white);
            color: white;
        }
    </style>
</head>

<body class="common-home layout-1">
    <nav id="top">
        <div class="container">

            <div class="container-top">

                <div class="lang-curr">
                   <center>
                       <ul type="none" id="til">
                           <li>
                               <a href="<?=URLMANAGER.'/uz'?>" class="badge badge-info">UZ</a>
                           </li>
                           <li>
                               <a href="<?=URLMANAGER.'/ru'?>" class="badge badge-warning actives">
                                   RU
                               </a>
                           </li>
                           <li>
                               <a href="<?=URLMANAGER.'/en'?>" class="badge badge-info">
                                   EN
                               </a>
                           </li>
                       </ul>
                   </center>
                </div>

               
                </div>

            </div>
        </div>

        </div>
    </nav>

    <div class="header-container">
        <div class="container">
            <div class="row">

                <div class="header-center">

                    <div class="content_header_topleft">
                        <div class="header-left">
                            <div class="col-sm-4 header-logo">
                                <div id="logo">
                                    <a href=""><img style="margin-top: -30px; width: 150px; height: 130px;" src="<?=$config['base']['url']."web/"?>images/2.png" title="Your Store" alt="Your Store" class="img-responsive" /></a>
                                </div>
                            </div>
                        </div>

                    <div class="head-right-bottom">
                        <div class="headertopright">
                            <div class="text2">
                                <div class="cms-data-info">
                                    <div  class="text2-dec"><?=$array['aloqa_'.$til];?></div>
                                    <a href="index2724.html?route=information/contact">
                                        <span class="hidden-xs hidden-sm hidden-md"><small>97-416-97-09</small></span>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <header>

                    <div class="container">
                        <div class="row">
                            <div class="header-main">

                                <div class="header-right">
                                    <!--  =============================================== Mobile menu start  =============================================  -->
                                    <div id="res-menu" class="main-menu nav-container1">
                                        <div class="nav-responsive"><span>Menu</span>
                                            <div class="expandable"></div>
                                        </div>
                                        <ul class="main-navigation">
                                            <?php foreach(getAll('bolimlar') as $r): ?>
                                                <li>
                                                    <?php 
                                                            $son = $r['id'];
                                                            if($r['id'] == 2){$son = 0; } ?>
                                                    <a href="<?=Url;?><?=strstr($r['nomi_uz'], ' ', true).'/'.$son; ?>">
                                                        <?=$r['nomi_'.$til]?>
                                                    </a>
                                                    <ul>
                                                    <?php foreach(getOneCol('subbolims', 'bolim_id', $r['id']) as $row): ?>
                                                        <li>
                                                            <a href="<?=Url?><?=strstr($row['nomi_uz'], ' ', true).'/'.$row['id']?>" class="activSub"><?=$row['nomi_'.$til];?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                    </ul>
                                                </li>
                                            <?php endforeach;  ?>
                                        </ul>
                                    </div>
                                    <!--  ================================ Mobile menu end   ======================================   -->
                                    <!-- ======= Menu Code END ========= -->

                                    <!-- ======= Menu Code START ========= -->

                                    <div class="head-right-top">
                                        <ul class="static_links">
                

                                            <ul class="nav navbar-nav">
                                                <?php foreach(getAll('bolimlar') as $r): ?>
                                                    <li class="top_level <?php if(getId('subbolims', 'bolim_id', $r['id'])){echo ' dropdown'; }?>">
                                                        <?php 
                                                            $son = $r['id'];
                                                            if($r['id'] == 2){$son = 0; } ?>
                                                        <a href="<?=Url;?><?=strstr($r['nomi_uz'], ' ', true).'/'.$son; ?>"><?=$r['nomi_'.$til]?></a>
                                                        <div class="dropdown-menu megamenu column3">
                                                            <div class="dropdown-inner">
                                                                <ul class="list-unstyled childs_1">
                                                                    <?php foreach(getOneCol('subbolims', 'bolim_id', $r['id']) as $row): ?>
                                                                        <li class="dropdown" style="width: 250px;">
                                                                            <a href="<?=Url.strstr($row['nomi_uz'], ' ', true).'/1';?>">
                                                                                <?=$row['nomi_'.$til];?> 
                                                                            </a>
                                                                        </li>
                                                                    <?php endforeach; ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php endforeach;  ?>
                                                <?php if(isset($_SESSION['ism'])): ?>
                                                    <li class="top_lavel">
                                                        <a href="<?=Url.'logout/2';?>">
                                                            <?php
                                                                    $ism = $_SESSION['ism'] ?? null;
                                                                    echo $array['chiqish_'.$til]."("."<strong style='color: red'>".$ism."</strong>".")";
                                                            ?>
                                                        </a>
                                                    </li>
                                                <?php elseif(isset($_SESSION['login'])): ?>
                                                    <li class="top_lavel">
                                                        <a href="<?=Url.'admin/';?>">
                                                            <?php
                                                                    $ism = $_SESSION['login'] ?? null;
                                                                    echo "<strong style='color: cyan; font-weight: bold'>".$_SESSION['login']."</strong>";
                                                            ?>
                                                        </a>
                                                    </li>
                                                <?php endif; ?>
                                            </ul>

                                            <div class="header-topr">
                                                <div class="head">
                                                    <div class="dropdown myaccount"><a href="indexe223.html?route=account/account" title="My Account" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?=$array['kabinet_'.$til];?></span> <!--<i class="fa fa-angle-down" aria-hidden="true"></i>--></a>
                                                        <ul class="dropdown-menu dropdown-menu-right myaccount-menu">
                                                            <li><a href="<?=Url.'Biz/4'?>"><?=$array['reg_'.$til];?></a></li>
                                                            <li><a href="<?=Url.'kabinet/2';?>"><?=$array['login_'.$til];?></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header-content">
                        <nav class="nav-container" role="navigation">

                            <div class="nav-inner container">
                                <div class="row">
                                    <div class="content_header_top"> </div>
                                    <div class="serach-inner">

    

                                    </div>
                                </div>
                            </div>

                        </nav>
                    </div>

                </header>
            </div>
        </div>
    </div>
    <div class="content-top-breadcum">
        <div class="container">
            <div class="row">
                <div id="title-content">
                </div>
            </div>
        </div>
    </div>
    <!-- ======= Quick view JS ========= -->
    <script>
        function quickbox() {
            if ($(window).width() > 767) {
                $('.quickview-button').magnificPopup({
                    type: 'iframe',
                    delegate: 'a',
                    preloader: true,
                    tLoading: 'Loading image #%curr%...',
                });
            }
        }
        jQuery(document).ready(function() {
            quickbox();
        });
        jQuery(window).resize(function() {
            quickbox();
        });
    </script>
    
    <?php if(isset($_GET['view']) && $_GET['view'] == 'Bosh'): ?>
        
        <?php include_once($config['base']['path']."views/pages/index.php")?>
    <?php  elseif(isset($_GET['view'])): ?>
        <?php $view = explode('/', $_GET['view']); ?>
        <?php include_once($config['base']['path']."views/pages/".$view[0].".php")?>      
    <?php else: ?>
        <?php include_once($config['base']['path']."views/pages/index.php")?>      
    <?php endif; ?>

                
   <footer>
        <div class="bottomfooter">
            <div class="container">
                <div class="row">
                    <p class="powered">Oltiariq MCHJ <a href="">Textil</a>Yil &copy; 2019</p>

                </div>
            </div>
        </div>

    </footer>


    
</body>
</html>