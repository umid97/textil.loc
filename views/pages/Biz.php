<?php if(isset($_SESSION['ism']) && isset($_SESSION['login'])): ?>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1 style="text-align:center; box-shadow: 0 0 5px black; font: 24px 'Times New Roman';background: #F3CACA;padding: 10px; margin: 20px; border-bottom: 4px solid #083F9B"><?=$array['bizz_'.$til];?></h1>
			</div>
		</div>
	</div>
<?php else: ?>
<aside class="container">
	<div class="row col-lg-12">
		<h4 align="justify"></h4>
		<form action="<?=Url.'contact/1'?>" method="post">
			<label>
				<strong><?=$array['fish_'.$til];?></strong>
				<input autocomplete="off" size="100" type="text" name="ism"  class="form-control" required>
			</label><hr>
			<label>
				<strong><?=$array['tel_'.$til]?></strong>
				<input autocomplete="off" type="text" name="tel" placeholder="+99897-995-66-93" pattern="+[0-9]{5}-[0-9]{3}-[0-9]{2}-[0-9]{2}" class="form-control" required size="100">
			</label><hr>
			<label>
				<strong><?=$array['manzil_'.$til];?></strong>
				<textarea name="manzil" id="text" cols="100" rows="10" required class="form-control"></textarea>
			</label><hr>
			<label>
				<strong id="conten"><?=$array['ka_'.$til];?></strong>
				<input autocomplete="off" type='text' name="login" required id="login" size="100" class="form-control">
			</label><br><hr>
			<label>
				<strong>Captcha 
					<img  src="<?=$config['base']['url']."views/users_pages/captcha.php"?>" alt=""></strong>
				<input autocomplete="off" type='text' required name="captcha" id="captcha" size="100" class="form-control">
			</label><br><hr>
			<input type="submit" id="tug1" name="ok" value="Yuborish" class="btn btn-success">
		</form>
		<hr>
	</div>
</aside>
<?php endif;  ?>
<script src="<?=$config['base']['url']."web/"?>js/jQuery.js" type="text/javascript"></script>
<script type="text/javascript">
  $(function(){
    $('#login').change(function(){
      $.ajax({
        url: '<?=$config['base']['url']."views/pages/test.php"?>',
        type: 'POST',
        data: {'nat': $(this).val()},
        success: function(a){
          if(a == 'true'){
            var t = 'Login mavjud. Qaytadan kiriting.';
            $('#login').css('background','red');
            $('#conten').text(t);
            $('#tug1').css('display','none');
          }
          if(a == 'false'){
            $('#login').css('background','white');
            $('#conten').text('To`g`ri!');
            $('#tug1').css('display','block');

          }
        },
        error: function(e){
          confirm(e.error);
        }
      })
    })
  });
</script>