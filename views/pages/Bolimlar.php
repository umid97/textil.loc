
<div class="container ">
    <div class="row">
        <aside id="column-left" class="col-sm-3 hidden-xs">
            <div class="box">
                <div class="box-heading"><?=$array['bolim_'.$til]?></div>
                
            </div>
            
            <div class="box latest">
                <div class="box-heading"><?=$array['m_'.$til];?></div>

                <div class="box-content">
                    <div class="customNavigation">
                        <a class="fa prev"></a>
                        <a class="fa next"></a>
                    </div>

                    <div class="box-product productbox-grid" id="latest-grid">
                        <?php foreach(getThere(3) as $r): ?>
                            <div class="product-items">
                                <div class="product-block product-thumb transition">
                                    <div class="product-block-inner">

                                        <div class="image">
                                            <a href="<?=Url.'One/'.$r['id']?>">
                                                <img style='width: 100px;' src="<?=$config['base']['url']."web/images/".$r['rasm1']?>" title="<?=$r['nomi_'.$til]?>" alt="<?=$r['nomi_'.$til]?>" class="img-responsive reg-image" />
                                                <img style='width: 100px;' class="img-responsive hover-image" src="<?=$config['base']['url']."web/images/".$r['rasm2']?>" title="<?=$r['nomi_'.$til]?>" alt="<?=$r['nomi_'.$til]?>" />
                                            </a>

                                            <div class="rating">
                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                            </div>
                                        </div>

                                        <div class="product-details">

                                            <div class="caption">
                                                <h4><a href="<?=Url."One/".$r['id'];?>"><?=$r['nomi_'.$til]?></a></h4>

                                                <p class="price">
                                                    <?=$r['narx']?>
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <span class="latest_default_width" style="display:none; visibility:hidden"></span>

            <div id="banner0" class="single-banner">
                <div class="item">
                    <img src="<?=$config['base']['url']."web/image/cache/catalog/"?>left-banner-261x340.jpg" alt="Left Banner" class="img-responsive" />
                </div>
            </div>
            <script type="text/javascript">
                <!--
                $('.bannercarousel').owlCarousel({
                    items: 1,
                    autoPlay: 3000,
                    singleItem: true,
                    navigation: false,
                    pagination: true,
                    transitionStyle: 'fade'
                });
                -->
            </script>
        </aside>
        <div id="content" class="col-sm-9">
            <div class="row category_thumb">
                <div class="col-sm-2 category_img"><img style="height: 300px!important" src="<?=$config['base']['url']."web/images/"?>img1.jpg" alt="Bo'limlar" title="Bo'limlar" class="img-thumbnail" /></div>
                <div class="col-sm-10 category_description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</div>
            </div>
            <div class="category_filter">
                <div class="col-md-4 btn-list-grid">
                    <div class="btn-group">
                        <button type="button" id="grid-view" class="btn btn-default grid" data-toggle="tooltip" title="Grid"><i class="fa fa-th"></i></button>
                        <button type="button" id="list-view" class="btn btn-default list" data-toggle="tooltip" title="List"><i class="fa fa-th-list"></i></button>
                    </div>
                </div>
            </div>

            <div class="row cat_prod">

                <?php if(isset($_GET['til']) && ($_GET['til'] == 'en' || $_GET['til'] == 'ru' || $_GET['til'] == 'uz') && isset($_SESSION['page'])): ?>
                   
                    <?php foreach(getInfoAll($_SESSION['page']) as $r): ?>
                        <div class="product-layout product-list col-xs-12">
                            <div class="product-block product-thumb">
                                <div class="product-block-inner">

                                    <div class="image">
                                    <a href="<?=Url.'One/'.$r['id']?>">
                                        <img style="width: 200px;" src="<?=$config['base']['url']."web/images/".$r['rasm1']?>" title="Aliquam Ac Neque" alt="Aliquam Ac Neque" class="img-responsive reg-image" />
                                        <img style="width: 200px;" class="img-responsive hover-image" src="<?=$config['base']['url']."web/images/".$r['rasm2']?>" title="<?=$r['nomi_'.$til]?>" alt="<?=$r['nomi_'.$til]?>" />
                                    </a>

                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                    </div>
                                    <?php if($r['chegirma'] > 0): ?>
                                        <span class="saleicon sale">Sale</span>
                                        <span class="percentsaving"><?=$r['chegirma']?>%<br></span>
                                    <?php endif; ?>
                                </div>

                                <div class="product-details grid">
                                    <div class="caption">
                                        <h4 class="name"><a href="<?=Url.'/'.$r['id'];?>" title="Aliquam Ac Neque"><?=$r['nomi_'.$til]?></a></h4>
                                        <p class="price">
                                             <?php 
                                                $c = ($r['narx'] * $r['chegirma']) / 100;
                                                $natija = $r['narx'] - $c;
                                            ?>
                                            <span class="price-new"><?=$natija?></span>

                                            <?php if($r['chegirma'] > 0): ?>
                                                <span class="price-old"><?=$r['narx'];?></span>
                                            <?php endif;  ?>

                                        </p>
                                    </div>
                                </div>

                                <div class="product-details list">
                                    <div class="caption">
                                        <h4 class="name"><a href="indexebf3.html?route=product/product&amp;path=20&amp;product_id=42" title="Aliquam Ac Neque"><?=$r['nomi_'.$til]?></a></h4>

                                        <p class="desc"><?=substr($r['izoh_'.$til], 0, 35);?></p>
                                        
                                    </div>
                                    <div class="list-right">
                                        <p class="price">
                                             <?php 
                                                if($r['chegirma'] > 0){
                                                    $c = ($r['narx'] * $r['chegirma']) / 100;
                                                    $natija = $r['narx'] - $c;
                                                }
                                                else{
                                                    $natija = $r['narx'];
                                                }
                                            ?>
                                            <span class="price-new"><?=$natija?></span>

                                            <?php if($r['chegirma'] > 0): ?>
                                                <span class="price-old"><?=$r['narx'];?></span>
                                            <?php endif;  ?>

                                        </p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        </div>
                    <?php endforeach; ?>

                <?php elseif(isset($_GET['til']) && ($_GET['til'] != 'en' || $_GET['til'] != 'ru' || $_GET['til'] != 'uz')): ?>
                        
                        <?php $_SESSION['page'] = $_GET['til']; ?>

                        <?php foreach(getInfoAll($_SESSION['page']) as $r): ?>
                            <div class="product-layout product-list col-xs-12">
                            <div class="product-block product-thumb">
                                <div class="product-block-inner">

                                    <div class="image">
                                    <a href="<?=Url.'One/'.$r['id']?>">
                                        <img style="width: 200px;" src="<?=$config['base']['url']."web/images/".$r['rasm1']?>" title="Aliquam Ac Neque" alt="Aliquam Ac Neque" class="img-responsive reg-image" />
                                        <img style="width: 200px;" class="img-responsive hover-image" src="<?=$config['base']['url']."web/images/".$r['rasm2']?>" title="<?=$r['nomi_'.$til]?>" alt="<?=$r['nomi_'.$til]?>" />
                                    </a>

                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                    </div>
                                    <?php if($r['chegirma'] > 0): ?>
                                        <span class="saleicon sale">Sale</span>
                                        <span class="percentsaving"><?=$r['chegirma']?>%<br></span>
                                    <?php endif; ?>
                                </div>

                                <div class="product-details grid">
                                    <div class="caption">
                                        <h4 class="name"><a href="<?=Url.'/'.$r['id'];?>" title="Aliquam Ac Neque"><?=$r['nomi_'.$til]?></a></h4>
                                        <p class="price">
                                            <?php 
                                                if($r['chegirma'] > 0){
                                                    $c = ($r['narx'] * $r['chegirma']) / 100;
                                                    $natija = $r['narx'] - $c;
                                                }
                                                else{
                                                    $natija = $r['narx'];
                                                }
                                                
                                            ?>
                                            <?=$natija?>
                                            <?php if($r['chegirma'] > 0): ?>
                                                <span class="price-old"><?=$r['narx'];?></span>
                                            <?php endif;  ?>
                                        </p>
                                    </div>
                                </div>

                                <div class="product-details list">
                                    <div class="caption">
                                        <h4 class="name"><a href="<?=Url.'One/'.$r['id']?>" title="<?=$r['nomi_'.$til]?>"><?=$r['nomi_'.$til]?></a></h4>

                                        <p class="desc"><?=substr($r['izoh_'.$til], 0, 35);?></p>
                                        
                                    </div>
                                    <div class="list-right">
                                        <p class="price">
                                             <?php 
                                                if($r['chegirma'] > 0){
                                                    $c = ($r['narx'] * $r['chegirma']) / 100;
                                                    $natija = $r['narx'] - $c;
                                                }
                                                else{
                                                    $natija = $r['narx'];
                                                }
                                            ?>
                                            <span class="price-new"><?=$natija?></span>

                                            <?php if($r['chegirma'] > 0): ?>
                                                <span class="price-old"><?=$r['narx'];?></span>
                                            <?php endif;  ?>

                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        </div>
                        <?php endforeach; ?>


                <?php else: ?>
                    
                    <?php foreach(getInfoAll() as $r): ?>
                        <div class="product-layout product-list col-xs-12">
                            <div class="product-block product-thumb">
                                <div class="product-block-inner">

                                    <div class="image">
                                    <a href="<?=Url.'One/'.$r['id']?>">
                                        <img style="width: 200px;" src="<?=$config['base']['url']."web/images/".$r['rasm1']?>" title="Aliquam Ac Neque" alt="Aliquam Ac Neque" class="img-responsive reg-image" />
                                        <img style="width: 200px;" class="img-responsive hover-image" src="<?=$config['base']['url']."web/images/".$r['rasm2']?>" title="<?=$r['nomi_'.$til]?>" alt="<?=$r['nomi_'.$til]?>" />
                                    </a>

                                    <div class="rating">
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                    </div>
                                    <?php if($r['chegirma'] > 0): ?>
                                        <span class="saleicon sale">Sale</span>
                                        <span class="percentsaving"><?=$r['chegirma']?>%<br></span>
                                    <?php endif; ?>
                                </div>

                                <div class="product-details grid">
                                    <div class="caption">
                                        <h4 class="name"><a href="<?=Url.'/'.$r['id'];?>" title="Aliquam Ac Neque"><?=$r['nomi_'.$til]?></a></h4>
                                        <p class="price">
                                             <?php 
                                                $c = ($r['narx'] * $r['chegirma']) / 100;
                                                $natija = $r['narx'] - $c;
                                            ?>
                                            <span class="price-new"><?=$natija?></span>

                                            <?php if($r['chegirma'] > 0): ?>
                                                <span class="price-old"><?=$r['narx'];?></span>
                                            <?php endif;  ?>
                                             <!-- <span class="price-old">$122.00</span> -->

                                        </p>
                                    </div>
                                </div>

                                <div class="product-details list">
                                    <div class="caption">
                                        <h4 class="name"><a href="indexebf3.html?route=product/product&amp;path=20&amp;product_id=42" title="Aliquam Ac Neque"><?=$r['nomi_'.$til]?></a></h4>

                                        <p class="desc"><?=substr($r['izoh_'.$til], 0, 100);?></p>
                                        
                                    </div>
                                    <div class="list-right">
                                        <p class="price">
                                             <?php 
                                                if($r['chegirma'] > 0){
                                                    $c = ($r['narx'] * $r['chegirma']) / 100;
                                                    $natija = $r['narx'] - $c;
                                                }
                                                else{
                                                    $natija = $r['narx'];
                                                }
                                            ?>
                                            <span class="price-new"><?=$natija?></span>

                                            <?php if($r['chegirma'] > 0): ?>
                                                <span class="price-old"><?=$r['narx'];?></span>
                                            <?php endif;  ?>
                                             <!-- <span class="price-old">$122.00</span> -->

                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        </div>
                    <?php endforeach; ?>
                
                <?php endif;  ?>
                    
                
            <div class="pagination-wrapper">
                <div class="col-sm-6 text-left page-link"></div>
                <div class="col-sm-6 text-right page-result">
                    <?php PaginationViews(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


