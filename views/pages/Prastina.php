<div class="container">
<div id="content" class="col-sm-12 all-blog">
    <div class="panel-default">
        <?php foreach(getThere(10, 'Prastina va Padadiyannik') as $r): ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="blog-left-content">
                        <div class="blog-image" style=" width: 100%; height: 450px;">
                            <img style="width: 100%; max-height: 100%" align="center" src="<?=$config['base']['url']."web/images/".$r['rasm1']?>" alt="Blogs" title="Blogs" class="img-thumbnail" />
                            <div class="post-image-hover" > </div>
                            <p class="post_hover"><a class="icon zoom" title="Click to view Full Image " href="<?=$config['base']['url']."web/images/".$r['rasm1']?>" data-lightbox="example-set"><i class="fa fa-search-plus"></i> </a><a class="icon readmore_link" title="Click to view Read More " href="<?=Url.'One/'.$r['id']?>"><i class="fa fa-link"></i></a></p>
                            <div class="blog-date"><?=$r['vaqt']?></div>
                        </div>
                    </div>

                    <div class="blog-right-content">
                        <h5 class="blog-title"><?=$r['nomi_'.$til]?></h5>
                        <div class="blog-desc"><?=$r['izoh_'.$til]?></div>
                        <div class="comment-wrapper">
                        </div>
                        <br><hr>
                        <div class="read-more">
                            <a href="<?=Url.'One/'.$r['id']?>"> <i class="fa fa-link"></i> <?=$array['tug_'.$til];?></a>
                        </div>

                    </div>
                </div>
            </div>
        <?php endforeach;  ?>
    </div>
</div>
</div>
