<?php if(isset($_POST['ok']) && ($_SESSION['son']) == $_POST['captcha']): ?>
	<?php 
		if(getInsert('contact', $_POST['ism'], $_POST['tel'], $_POST['manzil'], $_POST['login'])){
			$_SESSION['ism'] = $_POST['ism'];
			$_SESSION['login'] = $_POST['login'];
			$_SESSION['tel'] = $_POST['tel'];
			?>
				<script type="text/javascript">
					alert('Buyurtmangiz qabul qilindi!');
					window.location = '<?=Url.'kabinet/1'?>';
				</script>
			<?php
		}
		else{
			?>
				<script type="text/javascript">
					window.location="<?=$_SERVER['HTTP_REFERER']?>";
				</script>
			<?php
		}
	 ?>

<?php elseif(isset($_POST['save']) && $_SESSION['son'] == $_POST['captcha']): ?>
	<?php 
		if(getBuyurtma($_POST['fish'], $_POST['bolim'], $_POST['text'], $_POST['tel'], $_POST['login'], $_POST['manzil']) && getInsert('contact', $_POST['fish'], $_POST['tel'], $_POST['manzil'], $_POST['login'])){
			$_SESSION['ism'] = $_POST['fish'];
			$_SESSION['login'] = $_POST['login'];
			$_SESSION['tel'] = $_POST['tel'];
			unset($_SESSION['id']);
			unset($_SESSION['son']);
			?>
				<script type="text/javascript">
					alert('Buyurtmangiz qabul qilindi!');
					window.location = '<?=Url."kabinet/1"?>';
				</script>
			<?php
		}
		else{
			?>
				<script type="text/javascript">
					window.location = '<?=$_SERVER["HTTP_REFERER"]?>';
				</script>
			<?php
		}
	?>

<?php else: ?>
	<?php 
	?>
	<script type="text/javascript">
		window.location = '<?=$_SERVER['HTTP_REFERER']?>';
	</script>
<?php endif; ?>