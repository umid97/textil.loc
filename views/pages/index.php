 <div class="container">
        <div class="row home_row">
            <div id="content" class="col-sm-12">
                <div class="content-top">
                    <div class="main-slider">
                        <div id="spinner"></div>
                        <div id="slideshow0" class="owl-carousel" style="opacity: 1;">
                            <div class="item">
                                <img src="<?=$config['base']['url']."web/"?>images/img1.jpg" alt="MainBanner1" class="img-responsive"  style='width: 100%; height: 460px;'/>
                            </div>
                            <div class="item">
                                <img src="<?=$config['base']['url']."web/"?>images/img4.jpg" alt="MainBanner3" class="img-responsive" style='width: 100%; height: 460px;' />
                            </div>
                            <div class="item">
                                <img src="<?=$config['base']['url']."web/"?>images/img3.jpg" alt="MainBanner2" class="img-responsive" style='width: 100%; height: 460px;' />
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $('#slideshow0').owlCarousel({
                            items: 6,
                            autoPlay: 5000,
                            singleItem: true,
                            navigation: true,
                            navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
                            pagination: true,
                            transitionStyle: "fade"
                        });
                    </script>
                    <script type="text/javascript">
                        // Can also be used with $(document).ready()
                        $(window).load(function() {
                            $("#spinner").fadeOut("slow");
                        });
                    </script>
                </div>
                <div class="img-prd">
                    <!--<div class="left-image megnortabs">
  <div class="image-block">
    <span class="img"></span>
  </div>
</div>-->
                    <div class="hometab box">
                        <div class="tab-head">
                            <div class="hometab-heading box-heading"><?=$array['m_'.$til];?></div>
                            <div id="tabs" class="htabs">
                                <ul class='etabs'>

                                    <li class='tab'>
                                        <a href="#tab-latest"><?=$array['m_'.$til];?></a>
                                    </li>
                                    <li class='tab'>
                                        <a href="#tab-special"><?=$array['m2_'.$til];?></a>
                                    </li>
                                    <li class='tab'>
                                        <a href="#tab-bestseller"><?=$array['m1_'.$til]?></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div id="tab-latest" class="tab-content">
                            <div class="box">
                                <div class="box-content">
                                    <div class="customNavigation">
                                        <a class="fa prev"></a>
                                        <a class="fa next"></a>
                                    </div>

                                    <div class="box-product product-carousel" id="tablatest-carousel">

                                        <?php foreach(getOneCol('mahsulotlar', 'subbolim_nomi', 'Trikatej mahsulotlar', 10) as $r): ?>
                                            <div class="slider-item">
                                                <div class="product-block product-thumb transition">
                                                    <div class="product-block-inner">

                                                        <div class="image">
                                                            <a href="<?=Url."One/".$r['id']?>">
                                                                <img style="width: 100%; height: 250px;" src="<?=$config['base']['url']."web/"?>images/<?=$r['rasm1']?>" title="Proin Consectetur" alt="Proin Consectetur" class="img-responsive reg-image" />
                                                                <img style="width: 100%; height: 250px;" class="img-responsive hover-image" src="<?=$config['base']['url']."web/"?>images/<?=$r['rasm2']?>" title="<?=$r['nomi_'.$til]?>" alt="Proin Consectetur" />
                                                            </a>
                                                            <?php if($r['chegirma'] > 0): ?>
                                                                <span class="saleicon sale">Chegirma</span>
                                                                <span class="percentsaving"><?=$r['chegirma']?>% <br><!--off--></span>
                                                            <?php endif; ?>'' 

                                                            <div class="rating">
                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                            </div>

                                                        </div>

                                                        <div class="product-details">
                                                            <div class="caption">
                                                                <h4 class="name"><a href="index3d7a.html?route=product/product&amp;product_id=49" title="Proin Consectetur"><?=$r['nomi_'.$til]?></a></h4>
                                                                <p class="price">
                                                                    <?php 
                                                                        $c = ($r['narx'] * $r['chegirma']) / 100;
                                                                        $natija = $r['narx'] - $c;
                                                                    ?>
                                                                    <?=$natija?>
                                                                    <?php if($r['chegirma'] > 0): ?>
                                                                        <span class="price-old"><?=$r['narx'];?></span>
                                                                    <?php endif;  ?>
                                                                </p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach;  ?>
                                       
                                    </div>
                                </div>
                            </div>
                            <span class="tablatest_default_width" style="display:none; visibility:hidden"></span>
                        </div>

                        <div id="tab-bestseller" class="tab-content">
                            <div class="box">
                                <div class="box-content">

                                    <div class="box-product productbox-grid" id="tabbestseller-grid">
                                    <?php foreach(getOneCol('mahsulotlar', 'subbolim_nomi', 'Yosh bolalar kiyimlari', 10) as $row): ?>
                                        <div class="product-items">
                                            <div class="product-block product-thumb transition">
                                                <div class="product-block-inner">

                                                    <div class="image">
                                                        <a href="<?=Url."One/".$row['id']?>">
                                                            <img style="width: 100%; height: 250px;" src="<?=$config['base']['url']."web/"?>images/<?=$row['rasm1']?>" title="<?=$row['nomi_'.$til];?>" alt="<?=$row['nomi_'.$til];?>" class="img-responsive reg-image" />
                                                            <img style="width: 100%; height: 250px;" class="img-responsive hover-image" src="<?=$config['base']['url']."web/"?>images/<?=$row['rasm2']?>" title="<?=$row['nomi_'.$til];?>" alt="<?=$row['nomi_'.$til];?>" />
                                                        </a>

                                                        <?php if($row['chegirma'] > 0): ?>
                                                            <span class="saleicon sale">Chegirma</span>
                                                            <span class="percentsaving"><?=$row['chegirma']."%";?> <br><!--off--></span>
                                                        <?php endif; ?>

                                                        <div class="rating">
                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star off fa-stack-2x"></i></span>
                                                        </div>

                                                    </div>

                                                    <div class="product-details">
                                                        <div class="caption">
                                                            <h4 class="name"><a href="indexd21c.html?route=product/product&amp;product_id=47" title="Blandit Feugiat"><?=$row['nomi_'.$til]?></a></h4>
                                                            <p class="price">
                                                                <?php 
                                                                    $n = ($row['narx'] * $row['chegirma']) / 100;
                                                                    $sum = $row['narx'] - $n; 
                                                                ?>
                                                                <span class="price-new"><?=$sum;?></span>
                                                                <?php if($row['chegirma'] > 0): ?>
                                                                    <span class="price-old"><?=$row['narx'];?></span>
                                                                <?php endif; ?>
                                                                <span class="price-tax">Ex Tax: $70.00</span>
                                                            </p>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                        
                                    </div>
                                </div>
                            </div>
                            <span class="tabbestseller_default_width" style="display:none; visibility:hidden"></span>
                        </div>

                        <div id="tab-special" class="tab-content">
                            <div class="box">
                                <div class="box-content">
                                    <div class="customNavigation">
                                        <a class="fa prev"></a>
                                        <a class="fa next"></a>
                                    </div>

                                    <div class="box-product product-carousel" id="tabspecial-carousel">
                                        <?php foreach(getOneCol('mahsulotlar', 'subbolim_nomi', 'Ayollar liboslari', 10) as $row1): ?>
                                            <div class="slider-item">
                                                <div class="product-block product-thumb transition">
                                                    <div class="product-block-inner">

                                                        <div class="image">
                                                            <a href="<?=Url.'One/'.$row1['id']?>">
                                                                <img style="width: 100%; height: 250px;" src="<?=$config['base']['url']."web/"?>images/<?=$row1['rasm1']?>" title="<?=$row['nomi_'.$til];?>" alt="<?=$row['nomi_'.$til];?>" class="img-responsive reg-image" />
                                                                <img style="width: 100%; height: 250px;" class="img-responsive hover-image" src="<?=$config['base']['url']."web/"?>images/<?=$row1['rasm2']?>" title="<?=$row['nomi_'.$til];?>" alt="<?=$row['nomi_'.$til];?>" />
                                                            </a>

                                                            
                                                            <?php if($row1['chegirma'] > 0): ?>
                                                                <span class="saleicon sale">Chegirma</span>
                                                                <span class="percentsaving"><?=$row1['chegirma']?>%<br><!--off--></span>
                                                            <?php endif; ?>
                                                            <div class="rating">
                                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            </div>

                                                        </div>

                                                        <div class="product-details">
                                                            <div class="caption">
                                                                <h4 class="name"><a href="indexbb02.html?route=product/product&amp;product_id=42" title="<?=$row['nomi_'.$til]?>"><?=$row['nomi_'.$til]?></a></h4>
                                                                <p class="price">
                                                                    <?php 
                                                                        $n = ($row1['narx'] * $row['chegirma']) / 100;
                                                                        $sum = $row1['narx'] - $n;
                                                                    ?>
                                                                    <span class="price-new"><?=$sum;?></span>
                                                                    <?php if($row1['chegirma'] > 0): ?>
                                                                        <span class="price-old"><?=$row1['narx']?></span>
                                                                    <?php endif; ?>
                                                                    <span class="price-tax">Ex Tax: $90.00</span>
                                                                </p>

                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                            <span class="tabspecial_default_width" style="display:none; visibility:hidden"></span>
                        </div>

                    </div>
                </div>
                <script type="text/javascript">
                    $('#tabs a').tabs();
                </script>
<div class="box special">
                    <div class="box-head">
                        <div class="box-heading"><?=$array['bahmal_'.$til];?></div>
                    </div>
                    <div class="box-content">
                        <div class="box-content-inner-special">
                            <div class="special-inner-content">
                                <div class="customNavigation">
                                    <a class="fa prev"></a>
                                    <a class="fa next"></a>
                                </div>

                                <div class="box-product product-carousel" id="special-carousel">
                                    <?php foreach(getOneCol('mahsulotlar', 'subbolim_nomi', 'Bahmal mahsulotlar', 10) as $r): ?>
                                        <div class="slider-item">
                                            <div class="product-block product-thumb transition">
                                                <div class="product-block-inner">
                                                    <div class="image">

                                                        <a href="<?=Url.'One/'.$r['id']?>">
                                                            <img width="100%" style="height: 250px;" src="<?=$config['base']['url']."web/"?>images/<?=$r['rasm1']?>" title="<?=$r['nomi_'.$til]?>" alt="<?=$r['nomi_'.$til]?>" class="img-responsive reg-image" />
                                                            <img class="img-responsive hover-image" src="<?=$config['base']['url']."web/"?>images/<?=$r['rasm2']?>" width="100%" style="height: 250px;" title="<?=$r['nomi_'.$til]?>" alt="<?=$r['nomi_'.$til]?>" />
                                                        </a>

                                                    
                                                        
                                                        <?php if($r['chegirma'] > 0): ?>
                                                            <span class="saleicon sale">Sale</span>
                                                            <span class="percentsaving"><?=$r['chegirma']?>%<br> <!--off--></span>
                                                        <?php endif; ?>
                                                        <div class="rating">
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                                                        </div>

                                                    </div>

                                                    <div class="product-details">

                                                        <div class="caption">
                                                            <h4 class="name"><a href="indexbb02.html?route=product/product&amp;product_id=42" title="<?=$r['nomi_'.$til]?>"><?=$r['nomi_'.$til]?></a></h4>
                                                            <p class="price">
                                                                <?php 
                                                                    $n = ($r['narx'] * $r['chegirma']) / 100;
                                                                    $sum = $r['narx'] - $n;
                                                                ?>
                                                                <span class="price-new"><?=$sum;?></span> 
                                                                
                                                                <?php if($r['chegirma'] > 0): ?>
                                                                    <span class="price-old"><?=$r['narx'];?></span>
                                                                <?php endif; ?>
                                                                <span class="price-tax">Ex Tax: $90.00</span>
                                                            </p>

                                                            
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="special_default_width" style="display:none; visibility:hidden"></span>
                <div class="product_banner">
                    <div class="product_img">
                        <a href="#" title="pbanner"><img class="prd_image" src="<?=$config['base']['url']."web/"?>image/catalog/About.jpg" alt=""></a>
                    </div>
                </div>
               

                <span class="featured_default_width" style="display:none; visibility:hidden"></span>

                <script type="text/javascript">
                    <!--

                    $(document).ready(function() {
                        $('.blogcarousel').owlCarousel({
                            items: 5,
                            singleItem: false,
                            navigation: true,
                            navigationText: ['<i class="fa prev"></i>', '<i class="fa next"></i>'],
                            pagination: true,
                            itemsDesktop: [1200, 4],
                            itemsDesktopSmall: [979, 3],
                            itemsTablet: [767, 2],
                            itemsMobile: [479, 1]
                        });
                    });
                    -->
                </script>
               
            </div>
        </div>

    </div>