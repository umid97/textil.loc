<?php
	$text = imagecreatetruecolor(100, 30);
	$col = imagecolorallocate($text, 25, 55, 55);

	imagefilledrectangle($text, 100, 0, imagesx($text), imagesy($text), $col);
	$t = mt_rand(10000, 99999);
	$black = imagecolorallocate($text, 255, 255, 255);
	imagestring($text, 50, 25, 7, $t, $black);
	header("Content-type: image/jpeg");
	imagejpeg($text);
	imagedestroy($text);

	session_start();
	$_SESSION['son'] = $t;

?>