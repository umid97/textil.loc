<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?=$array['foyda_'.$til];?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?=$array['title_'.$til];?>
                </div>
                <div class="panel-body">
                    <p><?=$array['content_'.$til];?></p>
                </div>
            </div>
        </div>
    </div>

</div>