<?php 
    
    $til = $_SESSION['til'] ?? 'uz';
    $error = $_SESSION['error_'.$til] ?? null;
    if(isset($_POST['tug'])){
        $login = htmlspecialchars(addslashes($_POST['login']));
        $ism = $_POST['ism'];
        login($ism, $login);
        header("Location: ../users/");
        exit;
        // echo "<pre>";
        //     print_r($_POST);
        // echo "</pre>";
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=width-device, initial-scale=1"> 
    <title>Document</title>
    <link rel="stylesheet" href="<?=$config['base']['url']."web/users_css/css/css/bootstrap.min.css"?>">
    <link rel="stylesheet" href="<?=$config['base']['url']."web/css/font-awesome.min.css"?>">
    <style type="text/css">
        .container{
            height: 250px;
            margin: 7% auto;
        }
        .col-sm-4{
            border: 1px solid red;
            border-radius: 25px;
            padding: 10px 25px;
            box-shadow: 0 0 5px red;
            background: #CF6565;
        }
        .col-sm-4:hover form center i{
            color: white;
            text-shadow: 0px 0px 5px #CC8F00;
        }
        i{
            font-size: 150px!important;
            color: #EAE4D7;
        }
        body{
            background: #EAC6C6;
        }
        p{
            font: bold 18px 'Times New Roman';
        }
        @media screen and (max-width: 565px){
            body{
            }
            .col-8{
                margin: auto;
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <p align="justify;"><?=$error?></p>
            </div>
        </div>
        <div class="row">
            <!-- <div class="col-sm-4">salom</div> -->

            <div class="col-sm-4 col-8 offset-sm-4">

                <form action="" method="post">
                    <center>
                        <p><?=$array['kabinets_'.$til];?></p>
                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                    </center>
                    <label><?=$array['fish_'.$til];?>
                        <input autocomplete="off" type="text" name="ism" autofocus class="form-control" required size="40">
                    </label>
                    <label><?=$array['ka_'.$til];?>
                        <input autocomplete="off" type="password" name="login" class='form-control' size="40" required>
                    </label><hr>
                    <input type="submit" name="tug" value="<?=$array['login_'.$til];?>" class='btn btn-success form-control'><hr>
                    <a href='<?='http://localhost/textil_prect/'?>' class='btn btn-warning form-control'><?=$array['chiqish_'.$til]?></a>
                </form>
            </div>
        </div>
    </div>
    
</body>
</html>