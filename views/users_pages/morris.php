<div id="page-wrapper">
    <div class="row">
		<div class="col-sm-12">
			<div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 ">
                            <div><p><?=$array['sen_'.$til];?></p></div>
                        </div>
                    </div>
                </div>
            </div>
			
		</div>
        <div class="row">
            <div class="col-sm-12">
                <table width="100%" class="table table-bordered" style="background: #ccc; color: black">
                    <thead>
                        <th><?=$array['fish_'.$til];?></th><th><?=$array['mahsulot_'.$til];?></th><th><?=$array['qqq_'.$til];?></th><th><?=$array['t_'.$til];?></th><th><?=$array['manzil_'.$til];?></th><th><?=$array['va_kel_'.$til]?></th><th><?=$array['login_'.$til];?></th><th><?=$array['date_'.$til]?></th><th><?=$array['btn_'.$til];?></th>
                    </thead>
                <?php foreach(getAdminSMS('zakaz', 'status', 'active', 'login', $_SESSION['login']) as $r): ?>
                    <tr style="background-color: black; color: white; text-align: center;">
                        <td><i class="fa fa-user" aria-hidden="true"></i><br> <?=$r['fish']?></td>
                        <td><i class="fa fa-database" aria-hidden="true"></i><br> <?=$r['mahsulot_nomi']?></td>
                        <td>
                            <i class="fa fa-comments" aria-hidden="true"></i><br>
                            <p><?=substr($r['izoh'], 0, 50).'..'?></p>
                        </td>
                        <td>
                            <i class="fa fa-phone-square" aria-hidden="true"></i><br>
                            <strong><?=$r['tel']?></strong>
                        </td>
                        <td>
                            <i class="fa fa-home" aria-hidden="true"></i><br>
                            <p><?=$r['manzil']?></p>
                        </td>
                        <td>
                            <i class="fa fa-calendar" aria-hidden="true"></i><br>
                            <data><?=$r['vaqt']?></data>
                        </td>
                        <td>
                            <i class="fa fa-sign-in" aria-hidden="true"></i><br>
                            <strong><?=$r['login']?></strong>
                        </td>
                        <td>
                            <i class="fa fa-clock-o" aria-hidden="true"></i><br>    
                            <data><?=$r['tamom_vaqt']?></data>
                        </td>
                        <td>
                            <a href="<?=URL.'forms/'.$r['id']?>" title="Admin bilan bog'lanish" class="btn btn-info btn-sm">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>