<div id="page-wrapper">
    <div class="row">
		<?php foreach(getOneInfo('xabar', 'id', $_GET['til']) as $r): ?>
			<div class="col-sm-12" style="background: orange">
				<h4>
					<i class="fa fa-user" aria-hidden="true"></i>
					<?=$r['fish']?>
				</h4>
				<p>
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					<strong><?=$r['xabar']?></strong>
				</p>
				<h5>
					<i class="fa fa-calendar-o" aria-hidden="true"></i>
					<big><?=$r['vaqt']?></big>
				</h5>
				
				<div>
					<a href="<?=URL.'delete/'.$r['id']?>" class="btn btn-danger btn-sm">O'chirish</a>
				</div>
				<hr>
			</div>
		<?php endforeach; ?>
    </div>
</div>