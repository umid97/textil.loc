<div id="page-wrapper">
    <div class="row">
		<?php foreach(getOneInfo('xabar_admin', 'id', $_GET['til']) as $r): ?>
			<div class="col-sm-12" style="background: orange">
				<hr>
				<p>
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					<strong><?=$r['xabar']?></strong>
				</p>
				<h5>
					<i class="fa fa-calendar-o" aria-hidden="true"></i>
					<big><?=$r['vaqt']?></big>
				</h5>
				
				<div>
					<a href="<?=URL.'forms/'.$r['id']?>" class='btn btn-success btn-sm'>Javob yuborish</a>
					<a href="<?=URL.'deletes/'.$r['id']?>" class="btn btn-danger btn-sm">O'chirish</a>
				</div>
				<hr>
			</div>
		<?php endforeach; ?>
    </div>
</div>